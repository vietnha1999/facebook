# Facebook

Facebook app được phát triển bằng flutter.

# Contributors
-   Nguyễn Văn Trung
-   Bùi Minh Tuấn
-   Bùi Phó Bền
-   Nguyễn Thị Thu Thành
-   Nguyễn Hữu Anh Việt

# Thiết bị phù hợp
    Android Nouget 7.1
    Resoultion size: 1280*720 (16:9)

## Tải các packages phụ thuộc

```flutter pub get```

## Packages đã sử dụng

```pubspec.yaml```

## Mô tả các thư mục

#### assets:
    Tài nguyên của dự án: ảnh, video, âm thanh, config...
#### actions, middlewares, reducers, stores:
    Quản lý trạng thái ứng dụng (redux).
#### globalComponents:
    Các widgets có khả năng sử dụng nhiều trong app.
#### screens:
    Các màn hình (tính năng) trong app.
#### config:
    Các config ứng với mỗi tính năng trong app (Ví dụ: giới hạn dưới (1900?) của năm sinh mà có thể đăng ký trong nhập ngày tháng năm sinh khi tạo tài khoản). Đọc từ file .json.
#### localComponents:
    Các widgets có khả năng chỉ sử dụng trong tính năng hiện tại.
#### services, api:
    Viết các http request và response tới server.
#### utils:
    Các tiện ích thường sử dụng: chuyển timestamp sang xâu thời gian mong muốn trong text thời gian đăng bài, kiểm tra một xâu có phải số điện thoại việt nam hay không, kiểm tra xem ngày tháng năm này có hợp lệ hay không...
#### values:
    Các hằng số sử dụng trong app (error code, success code, 1 là like còn 0 là chưa like(trong http response)...).
