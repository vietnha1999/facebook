import 'package:facebook/stores/exampleState.dart';

class SetExampleProductAction {
  final ExampleProduct exampleProduct;

  SetExampleProductAction(this.exampleProduct);
}

class SetUserUIStateAction {
  final UserUIState userUIState;

  SetUserUIStateAction(this.userUIState);
}

class SetDarkModeAction {
  final bool darkMode;

  SetDarkModeAction(this.darkMode);
}
