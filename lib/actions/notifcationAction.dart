import 'package:facebook/stores/notificationState.dart';

class SetListNotifications {
  final List<NotificationSlotState> listNotifcations;

  SetListNotifications(this.listNotifcations);
}
