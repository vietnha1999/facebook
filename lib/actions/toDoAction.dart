import 'package:facebook/stores/toDoState.dart';

class SetListAlbumsAction {
  final List<Album> listAlbums;

  SetListAlbumsAction(this.listAlbums);
}
