import 'dart:convert';
import 'package:facebook/actions/toDoAction.dart';
import 'package:facebook/services/toDoHttp.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/toDoState.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

ThunkAction<AppState> getAlbumByUserIdThunk(String id) {
  return (Store<AppState> store) async {
    String albums = await ToDoHttp.getAlbumByUserId(id);
    List<Album> listAlbums = (jsonDecode(albums) as List).map((i) => Album.fromJson(i)).toList();
    store.dispatch(SetListAlbumsAction(listAlbums));
  };
}
