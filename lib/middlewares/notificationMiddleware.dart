import 'dart:convert';
import 'dart:developer';

import 'package:facebook/actions/notifcationAction.dart';
import 'package:facebook/api/notification/notificationApi.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/notificationState.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

ThunkAction<AppState> getNotificationsThunk() {
  return (Store<AppState> store) async {
    log("getNotificationsThunk");
    String _list = (await getNotifications()).body;
    log(_list);
    List<dynamic> list = jsonDecode(_list)['data'];
    List<NotificationSlotState> listNotifications = (list).map((i) => NotificationSlotState.fromJson(i)).toList();
    store.dispatch(SetListNotifications(listNotifications));
  };
}