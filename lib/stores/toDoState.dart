class Album {
  final int userId;
  final int id;
  final String title;

  Album({this.userId, this.id, this.title});

  factory Album.initial() {
    return Album(userId: null, id: null, title: null);
  }

  Album copyWith({int userId, int id, String title}) => Album(
      userId: userId ?? this.userId,
      id: id ?? this.id,
      title: title ?? this.title);

  Album.fromJson(Map<String, dynamic> json)
      : userId = json['userId'],
        id = json['id'],
        title = json['title'];

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Album &&
          userId == other.userId &&
          id == other.id &&
          title == other.title;

  @override
  int get hashCode => userId.hashCode ^ id.hashCode ^ title.hashCode;
}
