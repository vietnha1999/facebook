import 'dart:developer';

import 'package:facebook/actions/userInfoAction.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:redux/redux.dart';

final friendsReducer = combineReducers<List<UserInfoState>>({
  TypedReducer<List<UserInfoState>, SetFriendsAction>(_setFriendsAction),
});

List<UserInfoState> _setFriendsAction(List<UserInfoState> userInfo, SetFriendsAction action) {
  return action.friends;
}