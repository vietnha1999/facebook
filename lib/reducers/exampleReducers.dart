import 'package:facebook/actions/exampleActions.dart';
import 'package:facebook/stores/exampleState.dart';
import 'package:redux/redux.dart';

final exampleReducer = combineReducers<ExampleProduct>({
  TypedReducer<ExampleProduct, SetExampleProductAction>(_setExampleProductReducer),
});

ExampleProduct _setExampleProductReducer(
    ExampleProduct exampleProduct, SetExampleProductAction action) {
  return (exampleProduct == action.exampleProduct)
      ? exampleProduct
      : action.exampleProduct;
}

final userUIReducer = combineReducers<UserUIState>({
  TypedReducer<UserUIState, SetDarkModeAction>(_changeDarkModeReducer),
});

UserUIState _changeDarkModeReducer(
    UserUIState userUIState, SetDarkModeAction action) {
  return (userUIState.darkMode == action.darkMode) ? userUIState : userUIState.copyWith(darkMode: action.darkMode);
}

