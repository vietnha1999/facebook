import 'package:facebook/reducers/deviceInfo.dart';
import 'package:facebook/reducers/friendsReducer.dart';
import 'package:facebook/reducers/notificationReducer.dart';
import 'package:facebook/reducers/todoReducer.dart';
import 'package:facebook/reducers/userInfoReducer.dart';
import 'package:facebook/stores/appState.dart';
import 'exampleReducers.dart';

AppState appReducer(AppState state, dynamic action) => AppState(
  exampleProduct: exampleReducer(state.exampleProduct, action),
  userUIState: userUIReducer(state.userUIState, action),
  deviceInfo: deviceInfoReducer(state.deviceInfo, action),
  listAlbums: listAlbumsReducer(state.listAlbums, action),
  listNotifications: listNotificationsReducer(state.listNotifications, action),
  userInfo: userInfoReducer(state.userInfo, action),
  friends: friendsReducer(state.friends, action)
);
