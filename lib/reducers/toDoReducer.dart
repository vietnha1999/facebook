import 'package:collection/collection.dart';
import 'package:facebook/actions/toDoAction.dart';
import 'package:facebook/stores/toDoState.dart';
import 'package:redux/redux.dart';

final listAlbumsReducer = combineReducers<List<Album>>({
  TypedReducer<List<Album>, SetListAlbumsAction>(_setListAlbumsReducer),
});

List<Album> _setListAlbumsReducer(
    List<Album> listAlbums, SetListAlbumsAction action) {
  return (listAlbums.length == action.listAlbums.length && DeepCollectionEquality().equals(listAlbums, action.listAlbums))
      ? listAlbums
      : action.listAlbums;
}
