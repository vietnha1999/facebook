import 'dart:developer';

import 'package:facebook/actions/deviceInfoAction.dart';
import 'package:facebook/actions/userInfoAction.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:redux/redux.dart';

final userInfoReducer = combineReducers<UserInfoState>({
  TypedReducer<UserInfoState, SetUserInfoAction>(_setUserInfoAction),
});

UserInfoState _setUserInfoAction(UserInfoState userInfo, SetUserInfoAction action) {
  return (userInfo == action.userInfo) ? userInfo : userInfo.copyWith(avatar: action.userInfo.avatar, username: action.userInfo.username);
}