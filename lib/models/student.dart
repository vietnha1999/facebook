class Student  {
	String name;
	int age;

	Student({this.name, this.age});

	factory Student.fromJson(Map<String, dynamic> json) {
		return Student(
			name: json['name'],
			age: json['age'],
		);
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['name'] = this.name;
		data['age'] = this.age;
		return data;
	}


}
