import 'package:http/http.dart' as http;

const String URL_ALBUMS = "https://jsonplaceholder.typicode.com/albums";

class ToDoHttp {
  static Future<String> getAllAlbums() async {
    return await http.read(URL_ALBUMS);
  }
  static Future<String> getAlbumByUserId(String id) async {
    http.Response response = await http.get(URL_ALBUMS + "?userId=" + id);
    return response.body;
  }
}
