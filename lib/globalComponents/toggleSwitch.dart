import 'dart:developer';
import 'package:flutter/material.dart';

class ToggleSwitch extends StatefulWidget {
  final bool initSwitch;
  final Color activeColor;
  final Color activeTrackColor;
  final Function(bool) callback;
  ToggleSwitch({this.initSwitch, this.activeColor, this.activeTrackColor, this.callback});
  @override
  _ToggleSwitchState createState() => _ToggleSwitchState();
}

class _ToggleSwitchState extends State<ToggleSwitch> {
  bool isSwitch;
  @override
  void initState() {
    super.initState();
    isSwitch = widget.initSwitch ?? true;
  }
  @override
  Widget build(BuildContext context) {
    return Switch(
      value: isSwitch,
      onChanged: (value) {
        log("Tap toggle switch");
        if (widget.callback != null)
          widget.callback(value);
        setState(() => isSwitch = value);
      },
      activeColor: widget.activeColor ?? Colors.blue,
      activeTrackColor: widget.activeTrackColor ?? Colors.blue[100],
    );
  }
}