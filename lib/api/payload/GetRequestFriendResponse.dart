import 'package:facebook/api/payload/GetRequestFriendData.dart';

class GetRequestFriendResponse {
  int code;
  String message;
  GetRequestFriendData data;

  GetRequestFriendResponse({this.code, this.message, this.data});

  factory GetRequestFriendResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      return GetRequestFriendResponse(
        code: json['code'] as int,
        message: json['message'] as String,
        data: GetRequestFriendData.fromJson(json['data']),
      );
  } else return GetRequestFriendResponse(
    code: json['code'] as int,
        message: json['message'] as String
  );
  }

  @override
  String toString() {
    return '{ ${this.code}, ${this.message} , ${this.data} }';
  }
}