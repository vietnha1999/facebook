import 'package:facebook/api/payload/GetFriendData.dart';
import 'package:facebook/api/payload/GetFriendData.dart';

class GetFriendResponse {
  int code;
  String message;
  GetFriendData data;

  GetFriendResponse({this.code, this.message, this.data});

  factory GetFriendResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      return GetFriendResponse(
        code: json['code'] as int,
        message: json['message'] as String,
        data: GetFriendData.fromJson(json['data']),
      );
  } else return GetFriendResponse(
    code: json['code'] as int,
        message: json['message'] as String
  );
  }

  @override
  String toString() {
    return '{ ${this.code}, ${this.message} , ${this.data} }';
  }
}