import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/api/payload/SetRequestFriendData.dart';

class SetRequestFriendResponse {
  int code;
  String message;
  SetRequestFriendData data;

  SetRequestFriendResponse({this.code, this.message, this.data});

  factory SetRequestFriendResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      return SetRequestFriendResponse(
        code: json['code'] as int,
        message: json['message'] as String,
        data: SetRequestFriendData.fromJson(json['data']),
      );
    } else {
      return SetRequestFriendResponse(
        code: json['code'] as int,
        message: json['message'] as String
      );
    }
  }

  @override
  String toString() {
    return '{ ${this.code}, ${this.message} , ${this.data} }';
  }
}