class ResponseCode {
  int code;
  String message;

  ResponseCode({this.code, this.message});

  factory ResponseCode.fromJson(Map<String, dynamic> json) {
    return ResponseCode(
        code: json['code'] as int, message: json['message'] as String);
  }

  @override
  String toString() {
    return '{ ${this.code} , ${this.message} }';
  }
}
