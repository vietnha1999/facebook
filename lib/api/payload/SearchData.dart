import 'package:facebook/api/payload/MediaData.dart';
import 'package:facebook/api/payload/UserData.dart';

class SearchData {
  final int id;
  final String content;
  final String createTime;
  final String editTime;
  final String emotions;
  final String banned;
  final bool canComment;
  int liked;
  int comment;
  final List<MediaData> media;
  final UserData author;
  String is_liked;

  SearchData({
      this.id,
      this.content,
      this.createTime,
      this.editTime,
      this.emotions,
      this.banned,
      this.canComment,
      this.liked,
      this.comment,
      this.author,
      this.media,
      this.is_liked});

  factory SearchData.fromJson(Map<String, dynamic> json) {
    var tagObjsJson = json['media'] as List;
    List<MediaData> _media =
        tagObjsJson.map((tagJson) => MediaData.fromJson(tagJson)).toList();

    return SearchData(
      id: json['id'] as int,
      content: json['content'] as String,
      createTime: json['createTime'] as String,
      editTime: json['editTime'] as String,
      emotions: json['emotions'] as String,
      banned: json['banned'] as String,
      canComment: json['canComment'] as bool,
      is_liked: json['is_liked'] as String,
      liked: json['liked'] as int,
      comment: json['comment'] as int,
      author: UserData.fromJson(json['author']),
      media: _media,
    );
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.content}, ${this.createTime}, ${this.editTime}, ${this.emotions}, ${this.banned} , ${this.canComment}, ${this.liked}, ${this.is_liked}, ${this.comment}, ${this.author}, ${this.media} }';
  }
}
