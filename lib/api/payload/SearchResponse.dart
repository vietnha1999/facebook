import 'package:facebook/api/payload/SearchData.dart';

class SearchResponse {
  int code;
  String message;
  List<SearchData> data;

  SearchResponse({this.code, this.message, this.data});

  factory SearchResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      var tagObjsJson = json['data'] as List;
      List<SearchData> _data = tagObjsJson.map((tagJson) => SearchData.fromJson(tagJson)).toList();

      return SearchResponse(
        code: json['code'] as int,
        message: json['message'] as String,
        data: _data
      );
    } else {
      return SearchResponse(
        code: json['code'] as int,
        message: json['message'] as String
      );
    }
  }

  @override
  String toString() {
    return '{ ${this.code}, ${this.message} , ${this.data} }';
  }
}