class FriendsData {
  final int id;
	final String username;
	final String avatar;
	final int same_friends;
	final String created;

  // ignore: non_constant_identifier_names
  FriendsData({this.id, this.username, this.avatar, this.same_friends, this.created});

  factory FriendsData.fromJson(Map<String, dynamic> json) {
    return FriendsData(
        id: json['id'] as int,
        username: json['username'] as String,
        avatar: json['avatar'] as String,
        same_friends: json['same_friends'] as int,
        created: json['created'] as String);
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.username}, ${this.avatar}, ${this.same_friends}, ${this.created} }';
  }
}
