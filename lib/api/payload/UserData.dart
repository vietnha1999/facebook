class UserData {
  final int id;
  final String name;
  final String avatar;

  UserData({this.id, this.name, this.avatar});

  factory UserData.fromJson(Map<String, dynamic> json) {
      return UserData(
        id: json['id'] as int,
        name: json['name'] as String,
        avatar: json['avatar'] as String,
      );
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.name} , ${this.avatar} }';
  }
}