import 'package:facebook/api/payload/FriendsData.dart';

class GetRequestFriendData {
  final List<FriendsData> request;
  final int total;

  GetRequestFriendData({this.request, this.total});

  factory GetRequestFriendData.fromJson(Map<String, dynamic> json) {
    var tagObjsJson = json['request'] as List;
    List<FriendsData> _friends =
        tagObjsJson.map((tagJson) => FriendsData.fromJson(tagJson)).toList();
    return GetRequestFriendData(
      request: _friends,
      total: json['total'] as int,
    );
  }

  @override
  String toString() {
    return '{ ${this.request}, ${this.total} }';
  }
}
