class SetRequestFriendData {
  final int request_friends;

  SetRequestFriendData({this.request_friends});

  factory SetRequestFriendData.fromJson(Map<String, dynamic> json) {
    return SetRequestFriendData(
        request_friends: json['request_friends'] as int,
    );
  }

  @override
  String toString() {
    return '{ ${this.request_friends} }';
  }
}
