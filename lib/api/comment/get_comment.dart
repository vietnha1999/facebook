import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

Future<GetCommentResponse> get_comment(
    String authorization, int id, int index, int count) async {
  var client = new http.Client();
  print(id);
  http.Response rs = await client.post(
    Constants.Root + 'api/get_comment',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': authorization,
    },
    body:
        jsonEncode(<String, dynamic>{'id': id, 'index': index, 'count': count}),
  );
  print(GetCommentResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
  return GetCommentResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}

class GetCommentResponse {
  int code;
  String message;
  List<GetCommentData> data;

  GetCommentResponse({this.code, this.message, this.data});

  factory GetCommentResponse.fromJson(Map<String, dynamic> json) {
    var tagObjsJson = json['data'] as List;
      List<GetCommentData> _data = tagObjsJson.map((tagJson) => GetCommentData.fromJson(tagJson)).toList();
    if (json['data'] != null) {
      return GetCommentResponse(
        code: json['code'] as int,
        message: json['message'] as String,
        data: _data,
      );
    } else
      return GetCommentResponse(
          code: json['code'] as int, message: json['message'] as String);
  }

  @override
  String toString() {
    return '{ ${this.code}, ${this.message} , ${this.data} }';
  }
}

class GetCommentData {

  final int id;
	final String comment;
	final String created;
	final PostData poster;

  // ignore: non_constant_identifier_names
  GetCommentData({this.id, this.comment, this.created, this.poster});

  factory GetCommentData.fromJson(Map<String, dynamic> json) {
    return GetCommentData(
        id: json['id'] as int,
        comment: json['comment'] as String,
        created: json['created'] as String,
        poster: PostData.fromJson(json['poster']));
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.comment}, ${this.created}, ${this.poster}}';
  }

}

class PostData {
  final int id;
	final String name;
	final String avatar;

  // ignore: non_constant_identifier_names
  PostData({this.id, this.name, this.avatar});

  factory PostData.fromJson(Map<String, dynamic> json) {
    return PostData(
        id: json['id'] as int,
        name: json['name'] as String,
        avatar: json['avatar'] as String
        );
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.name}, ${this.avatar}}';
  }
}

