import 'dart:convert';

import 'package:facebook/api/comment/get_comment.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

Future<SetCommentResponse> set_comment(
    String authorization, String token, int id, String comment) async {
  var client = new http.Client();
  http.Response rs = await client.post(
    Constants.Root + 'api/set_comment',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': authorization,
    },
    body:
        jsonEncode(<String, dynamic>{'id': id, 'token': token, 'comment': comment}),
  );
  print(SetCommentResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
  return SetCommentResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}


class SetCommentResponse {
  int code;
  String message;
  GetCommentData data;

  SetCommentResponse({this.code, this.message, this.data});

  factory SetCommentResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      return SetCommentResponse(
        code: json['code'] as int,
        message: json['message'] as String,
        data: GetCommentData.fromJson(json['data']),
      );
    } else
      return SetCommentResponse(
          code: json['code'] as int, message: json['message'] as String);
  }

  @override
  String toString() {
    return '{ ${this.code}, ${this.message} , ${this.data} }';
  }
}

class SetCommentData {

  final int id;
	final String comment;
	final String created;
	final PostData poster;

  // ignore: non_constant_identifier_names
  SetCommentData({this.id, this.comment, this.created, this.poster});

  factory SetCommentData.fromJson(Map<String, dynamic> json) {
    return SetCommentData(
        id: json['id'] as int,
        comment: json['comment'] as String,
        created: json['created'] as String,
        poster: PostData.fromJson(json['poster']));
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.comment}, ${this.created}, ${this.poster}}';
  }

}

class PostData {
  final int id;
	final String name;
	final String avatar;

  // ignore: non_constant_identifier_names
  PostData({this.id, this.name, this.avatar});

  factory PostData.fromJson(Map<String, dynamic> json) {
    return PostData(
        id: json['id'] as int,
        name: json['name'] as String,
        avatar: json['avatar'] as String
        );
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.name}, ${this.avatar}}';
  }
}

