import 'dart:convert';
import 'package:facebook/api/payload/ResponseCode.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

// ignore: non_constant_identifier_names
Future<ResponseCode> set_block(String authorization, String token, int user_id, int type) async {
    var client = new http.Client();
    http.Response rs = await client.post(
      Constants.Root + 'api/set_block',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':
            authorization,
      },
      body: jsonEncode(<String, dynamic>{
        'token': token,
        'user_id': user_id,
        'type': type
      }),
    );
    print(ResponseCode.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
    return ResponseCode.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}