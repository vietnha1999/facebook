import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:facebook/common/constants.dart' as Constants;


Future<Response> getNotifications() async {
  var client = new http.Client();
  http.Response rs = await client.post(
    Constants.Root + 'api/notification',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': Constants.token,
    },
    body: jsonEncode(<String, String>{
      'token':  Constants.token,
    }),
  );
  log("getNotifications");
  return rs;
}

Future<Response> getUserInfo(String id) async {
  var client = new http.Client();
  http.Response rs = await client.post(
    Constants.Root + 'api/get_user_info',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': Constants.token,
    },
    body: jsonEncode(<String, String>{
      'token':  Constants.token,
      'user_id': id
    }),
  );
  log("getUserInfo");
  return rs;
}

Future<Response> getFriends(String id) async {
  var client = new http.Client();
  http.Response rs = await client.post(
    Constants.Root + 'api/get_user_friends',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization':  Constants.token,
    },
    body: jsonEncode(<String, String>{
      'token': Constants.token,
      'user_id': id,
      'index': '0',
      'count': '100'
    }),
  );
  log("getFriends");
  return rs;
}