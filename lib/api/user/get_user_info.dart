import 'dart:convert';

import 'package:facebook/api/payload/GetUserInfoResponse.dart';
import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

Future<GetUserInfoResponse> get_user_info(
    String authorization, String token, int user_id) async {
  var client = new http.Client();
  http.Response rs = await client.post(
    Constants.Root + 'api/get_user_info',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': authorization,
    },
    body: jsonEncode(<String, dynamic>{'token': token, 'user_id': user_id}),
  );
  print(GetUserInfoResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
  return GetUserInfoResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}
