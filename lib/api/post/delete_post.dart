import 'dart:convert';
import 'package:facebook/api/payload/ResponseCode.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

Future<ResponseCode> delete_post(String authorization, String token, int id) async {
    var client = new http.Client();
    http.Response rs = await client.post(
      Constants.Root + 'api/delete_post',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':
            authorization,
      },
      body: jsonEncode(<String, dynamic>{
        'token': token,
        'id': id,
      }),
    );
    print(ResponseCode.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
    return ResponseCode.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}