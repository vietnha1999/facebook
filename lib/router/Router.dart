import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:facebook/main.dart';
import 'package:facebook/screens/chat/chat.dart';
import 'package:facebook/screens/example/exampleScreen.dart';
import 'package:facebook/screens/example/homeScreen.dart';
import 'package:facebook/screens/example/toDoScreen.dart';
import 'package:facebook/screens/friend/friendtab/Friend.dart';
import 'package:facebook/screens/messenger/messengerCameraScreen.dart';
import 'package:facebook/screens/messenger/messengerChatScreen.dart';
import 'package:facebook/screens/messenger/messengerDev.dart';
import 'package:facebook/screens/friend/allfriend/AllFriend.dart';
import 'package:facebook/screens/messenger/messengerFakeData.dart';
import 'package:facebook/screens/messenger/messengerMainScreen.dart';
import 'package:facebook/screens/notification/notificationDev.dart';
import 'package:facebook/screens/comment/Comment.dart';
import 'package:facebook/screens/friend/allfriend/AllFriend.dart';
import 'package:facebook/screens/friend/friendtab/Friend.dart';
import 'package:facebook/screens/friend/suggestfiend/SuggestFriend.dart';
import 'package:facebook/screens/home/HomeTab.dart';
import 'package:facebook/screens/home/HomeTabNew.dart';
import 'package:facebook/screens/login/AfterRegister/LoginFb.dart';
import 'package:facebook/screens/login/UserHomePage.dart';
import 'package:facebook/screens/personal/Personal.dart';
import 'package:facebook/screens/personal/Setting.dart';
import 'package:facebook/screens/post/EmojiAndActivity.dart';
import 'package:facebook/screens/search/listpost/SearchPost.dart';
import 'package:facebook/screens/post/AddPost.dart';
import 'package:facebook/screens/login/signin.dart';
import 'package:facebook/screens/videoblack/videoblack.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/screens/messenger/messengerSettingScreen.dart';
import 'package:facebook/screens/messenger/messengerBoxUserScreen.dart';
import '../screens/home/HomeTabNew.dart';


const String EXAMPLE_SCREEN = '/exampleScreen';
const String EXAMPLE_RESULT = '/exampleResult';
const String HOME = "/home";
const String TO_DO_INPUT = "/toDoInput";
const String CHAT_SCREEN = "/chat";
const String CHAT_FRIEND = "/chatFriend";
const String ALL_FRIEND = "/allFriend";
const String SEARCH_POST = "/seachPost";
const String NOTIFICATION = "/notification";
const String MESSENGER_MAIN = "/messengerMain";
const String MESSENGER_CHAT = "/messengerChat";
const String EMOJI_AND_ACTIVITY = "/emojiAndActivity";
const String TAB_FRIEND = "/tabFriend";
const String ADD_POST = "/addPost";
const String SETTING_PERSONAL = "/settingPersonal";
const String PERSONAL_PAGE = "/personal";
const String COMMENT = "/comment";
const String SUGGEST_FRIEND = "/suggest_friend";
const String HOME_TAB = "/home_tab";
const String LOGIN_FB = "/login_fb";
const String SINGLE_POST = "/single_post";
const String BLACK_VIDEO = "/black_video";
const String MESSENGER_CAMERA = "/messengerCamera";
const String MESSENGER_SETTING = "/messengerSetting";
const String MESSENGER_BOX = "/MESSENGER_BOX";
const String SIGN_IN = "/sign_in";

class NavService {
  static final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();
  static Future<dynamic> navigateTo(String routeName) {
    return navigatorKey.currentState.pushNamed(routeName);
  }
}

class RouterNav {
  static Route<dynamic> generateRoute(RouteSettings settings) {

    switch (settings.name) {
      case ALL_FRIEND:
        return MaterialPageRoute(builder: (context) => AllFriend());
        case TAB_FRIEND:
        return MaterialPageRoute(builder: (context) => Friend());
      case SIGN_IN:
        return MaterialPageRoute(builder: (context) => signIn());
      case SEARCH_POST:
        return MaterialPageRoute(builder: (context) => SearchPost());
      case CHAT_SCREEN:
        return MaterialPageRoute(builder: (context) => ChatScreen());
      case EMOJI_AND_ACTIVITY:
        return MaterialPageRoute(builder: (context) => EmojiAndActivity());
      case ADD_POST:
        return MaterialPageRoute(builder: (context) => AddPost());
      case SETTING_PERSONAL:
        return MaterialPageRoute(builder: (context) => SettingPersonal());
      case PERSONAL_PAGE:
        var arguments = settings.arguments as Map;
        return MaterialPageRoute(builder: (context) => UserHomePage(user_id: arguments["id"]));
      case COMMENT:
        return MaterialPageRoute(builder: (context) => Comment());
      case SUGGEST_FRIEND:
        return MaterialPageRoute(builder: (context) => SuggestFriend());
      case HOME_TAB:
        return MaterialPageRoute(builder: (context) => HomeTabNew());
      case LOGIN_FB:
        return MaterialPageRoute(
                  builder: (context) => LoginFb());
      case CHAT_FRIEND:
        var arguments = settings.arguments as Map;
        return MaterialPageRoute(
            builder: (context) => ChatFriend(
                userId: arguments["userId"], friendId: arguments["friendId"]));
      case MESSENGER_MAIN:
        return MaterialPageRoute(
          // builder: (context) => MessengerDev()
          builder:(context) => MessengerMainScreen(countNewMessage: fakeCountNewMessage, dataFriend: fakeFriend, userAva: fakeUserAvatar)
        );
      case MESSENGER_CHAT:
        var arguments = settings.arguments as Map;
        return MaterialPageRoute(
          builder: (context) => MessengerChatScreen(zfriend: arguments['friend'])
        );
     case NOTIFICATION:
        return MaterialPageRoute(builder: (context) => NotificationDev());
      //   // return MaterialPageRoute(builder: (context) => MessengerDev());
      case BLACK_VIDEO:
        var arguments = settings.arguments as Map;
        return MaterialPageRoute(
            builder: (context) => VideoBlack(id: arguments["id"]));
      case MESSENGER_CAMERA:
        var arguments = settings.arguments as Map;
        return MaterialPageRoute(builder: (context) => MessengerCameraScreen(camera: arguments["camera"]));
      case MESSENGER_SETTING:
        var arguments = settings.arguments as Map;
        return MaterialPageRoute(builder: (context) => MessengerSettingScreen(userAvatar: arguments["avatar"]));
      case MESSENGER_BOX:
        var arguments = settings.arguments as Map;
        return MaterialPageRoute(builder: (context) => MessengerBoxUserScreen(userAvatar: arguments["avatar"], username: arguments["username"]));
      default:
        return MaterialPageRoute(builder: (context) => signIn());
    }
  }
}