import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';

class DeviceInfoBase {
  static Future<String> getDeviceUUID() async {
    String uuid;
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        uuid = build.androidId;
      } else if (Platform.isIOS) {
        var build = await deviceInfoPlugin.iosInfo;
        uuid = build.identifierForVendor;
      } else {
        uuid = "unknown";
      }
    } on PlatformException {
      uuid = "unknown";
      print("Error to get platform");
    }
    return uuid;
  }
}
