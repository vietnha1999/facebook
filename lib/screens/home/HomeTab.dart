// import 'dart:math';

// import 'package:facebook/api/friend/get_requested_friends.dart';
// import 'package:facebook/api/listpost/get_list_posts.dart';
// import 'package:facebook/api/payload/GetRequestFriendResponse.dart';
// import 'package:facebook/api/payload/SearchResponse.dart';
// import 'package:facebook/api/search/Search.dart';
// import 'package:facebook/router/Router.dart';
// import 'package:facebook/screens/friend/friendtab/FriendContainter.dart';
// import 'package:facebook/screens/search/listpost/SearchFuture.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:facebook/common/constants.dart' as Constants;
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

// class HomeTab extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: TabBarDemo(),
//     );
//   }
// }

// class TabBarDemo extends StatefulWidget {
//   @override
//   _TabBarDemoState createState() => _TabBarDemoState();
// }

// class _TabBarDemoState extends State<TabBarDemo>
//     with SingleTickerProviderStateMixin {
//   TabController _controller;
//   int _selectedIndex = 0;
//   Future<SearchResponse> searchResponse;
//   String token;
//   String authorization;
//   String keywork;
//   int user_id;
//   int index;
//   int count;
//   TextEditingController searchField;
//   SearchFuture searchFuture;
//   Future<GetRequestFriendResponse> getFriendResponse;

//   SearchResponse listPostResponse;
//   _TabBarDemoState();

//   List<Widget> list = [
//     Tab(
//       child: Text("Tất cả"),
//     ),
//     Tab(
//       child: Text("Bài viết"),
//     ),
//     Tab(
//       child: Text("Mọi người"),
//     ),
//     Tab(
//       child: Text("Ảnh"),
//     ),
//     Tab(
//       child: Text("Video"),
//     ),
//   ];
//   var refreshKey = GlobalKey<RefreshIndicatorState>();
//   var random;

//   Future<Null> refreshList() async {
//     refreshKey.currentState?.show(atTop: false);
//     await Future.delayed(Duration(seconds: 2));

//     setState(() {
//       // list = List.generate(random.nextInt(10), (i) => "Item $i");
//     });

//     return null;
//   }

//   @override
//   initState() {
//     super.initState();
//     random = Random();
//     authorization = Constants.authorization;
//     token = Constants.token;
//     user_id = 2;
//     searchResponse = get_list_post(token, token, user_id, index, count)
//         as Future<SearchResponse>;
//     getFriendResponse = get_requested_friends(authorization, token, user_id)
//         as Future<GetRequestFriendResponse>;
//     _controller = TabController(length: list.length, vsync: this);
//     _controller.addListener(() {
//       setState(() {
//         _selectedIndex = _controller.index;
//       });
//       print("Selected Index: " + _controller.index.toString());
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: DefaultTabController(
//         length: 5,
//         child: Scaffold(
//           appBar: AppBar(
//             backgroundColor: Colors.white,
//             title: Container(
//               height: 85.0,
//               decoration: BoxDecoration(
//                 color: Colors.white,
//                 // boxShadow: const [
//                 //   BoxShadow(
//                 //     color: Colors.black12,
//                 //     offset: Offset(0, 2),
//                 //     blurRadius: 4.0,
//                 //   ),
//                 // ],
//               ),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Expanded(
//                     child: Text(
//                       'facebook',
//                       style: const TextStyle(
//                         color: Color(0xFF1777F2),
//                         fontSize: 30.0,
//                         fontWeight: FontWeight.bold,
//                         letterSpacing: -1.2,
//                       ),
//                     ),
//                   ),
//                   Container(
//                     height: double.infinity,
//                     width: 60.0,
//                     child: null,
//                   ),
//                   Expanded(
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.end,
//                       children: [
//                         Container(
//                           width: 40,
//                           margin: const EdgeInsets.all(6.0),
//                           decoration: BoxDecoration(
//                             color: Colors.grey[200],
//                             shape: BoxShape.circle,
//                           ),
//                           child: IconButton(
//                             icon: Icon(Icons.search),
//                             iconSize: 25.0,
//                             color: Colors.black,
//                             onPressed: () => print('Search'),
//                           ),
//                         ),
//                         Container(
//                           width: 40,
//                           // margin: const EdgeInsets.all(6.0),
//                           decoration: BoxDecoration(
//                             color: Colors.grey[200],
//                             shape: BoxShape.circle,
//                           ),
//                           child: IconButton(
//                             icon: Icon(MdiIcons.facebookMessenger),
//                             iconSize: 25.0,
//                             color: Colors.black,
//                             onPressed: () => print('Search'),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//             bottom: new PreferredSize(
//               child: Container(
//                 // padding: EdgeInsets.only(top: 20),
//                 child: TabBar(
//                   isScrollable: true,
//                   unselectedLabelColor: Colors.grey,
//                   labelColor: Colors.blue,
//                   tabs: [
//                     Container(
//                       width: 40,
//                       height: 50,
//                       alignment: Alignment.center,
//                       child: Tab(
//                         child: Icon(
//                           Icons.home_outlined,
//                           // color: Color.fromRGBO(102, 102, 107, 1),
//                         ),
//                       ),
//                     ),
//                     Container(
//                       width: 40,
//                       height: 50,
//                       alignment: Alignment.center,
//                       child: Tab(
//                           child: Icon(
//                         MdiIcons.accountCircleOutline,
//                       )),
//                     ),
//                     Container(
//                       width: 40,
//                       height: 50,
//                       alignment: Alignment.center,
//                       child: Tab(
//                         child: Icon(Icons.ondemand_video),
//                       ),
//                     ),
//                     Container(
//                       width: 40,
//                       height: 50,
//                       alignment: Alignment.center,
//                       child: Tab(
//                         child: Icon(
//                           MdiIcons.bellOutline,
//                         ),
//                       ),
//                     ),
//                     Container(
//                       width: 40,
//                       height: 50,
//                       alignment: Alignment.center,
//                       child: Tab(
//                         child: Icon(
//                           Icons.menu,
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               preferredSize: Size.fromHeight(40.0),
//             ),
//           ),
//           backgroundColor: Colors.white,
//           body: new TabBarView(
//             children: <Widget>[
//               RefreshIndicator(
//                     key: refreshKey,
//         child:Container(
//                 child: Container(
//                   child: SingleChildScrollView(
//                     child: Column(
//                       children: [
//                         Container(
//                           child: Column(
//                             children: [
//                               Container(
//                                 padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
//                                 color: Colors.white,
//                                 child: Column(
//                                   children: [
//                                     Row(
//                                       children: [
//                                         Container(
//                                           padding:
//                                               EdgeInsets.fromLTRB(10, 5, 0, 0),
//                                           child: CircleAvatar(
//                                             radius: 25,
//                                             backgroundImage: NetworkImage(
//                                               Constants.fakeImage,
//                                             ),
//                                           ),
//                                         ),
//                                         Flexible(
//                                           child: TextField(
//                                             // textInputAction:
//                                             //     TextInputAction.newline,
//                                             readOnly: true,
//                                             onTap: () {
//                                               Navigator.pushNamed(
//                                                   context, ADD_POST);
//                                             },
//                                             style: TextStyle(
//                                                 fontSize: 15.0,
//                                                 color: Colors.black),
//                                             decoration: InputDecoration(
//                                               prefixIcon: Icon(
//                                                 Icons.search,
//                                                 color: Colors.black,
//                                               ),
//                                               filled: true,
//                                               fillColor: Color.fromRGBO(
//                                                   240, 242, 245, 1),
//                                               hintText: 'Bạn đang nghĩ gì?',
//                                               contentPadding:
//                                                   const EdgeInsets.only(
//                                                       left: 8.0, top: 11.0),
//                                               focusedBorder: OutlineInputBorder(
//                                                 borderSide: BorderSide(
//                                                     color: Colors.white),
//                                                 borderRadius:
//                                                     BorderRadius.circular(26.7),
//                                               ),
//                                               enabledBorder:
//                                                   UnderlineInputBorder(
//                                                 borderSide: BorderSide(
//                                                     color: Colors.white),
//                                                 borderRadius:
//                                                     BorderRadius.circular(26.7),
//                                               ),
//                                             ),
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
                        
                         
                        
//                         SearchFuture(
//                           searchResponse: searchResponse,
//                         ),
//                       ],
//                     ),
//                   ),
//                   color: Color.fromRGBO(201, 204, 209, 1),
//                 ),
//               ),
//               onRefresh: refreshList,
//       ),
              
//               Column(
//                 children: [
//                   Container(
//                     padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
//                     color: Color.fromRGBO(201, 204, 209, 1),
//                     child: Container(
//                       padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
//                       color: Colors.white,
//                       child: Column(
//                         children: [
//                           Row(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: [
//                               Container(
//                                 padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
//                                 child: Text(
//                                   "Bạn bè",
//                                   style: TextStyle(
//                                       fontSize: 32,
//                                       fontWeight: FontWeight.bold),
//                                 ),
//                               ),
//                               Expanded(
//                                 child: Text(""),
//                               ),
//                               Align(
//                                 alignment: Alignment.topRight,
//                                 child: Container(
//                                     padding: const EdgeInsets.symmetric(
//                                         horizontal: 0.0),
//                                     child: RawMaterialButton(
//                                       onPressed: () {
//                                         Navigator.pushNamed(
//                                             context, SEARCH_POST);
//                                       },
//                                       elevation: 2.0,
//                                       fillColor:
//                                           Color.fromRGBO(229, 230, 235, 1),
//                                       child: Icon(
//                                         Icons.search,
//                                         // size: 5.0,
//                                       ),
//                                       padding: EdgeInsets.all(4.0),
//                                       shape: CircleBorder(),
//                                     )),
//                               ),
//                             ],
//                           ),
//                           Row(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: [
//                               Container(
//                                 padding: EdgeInsets.only(left: 10),
//                                 child: FlatButton(
//                                   shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.circular(18.0),
//                                   ),
//                                   color: Color.fromRGBO(229, 230, 235, 1),
//                                   textColor: Colors.red,
//                                   padding: EdgeInsets.all(14.0),
//                                   onPressed: () {
//                                     Navigator.pushNamed(context, SUGGEST_FRIEND);
//                                   },
//                                   child: Text(
//                                     "Gợi ý",
//                                     style: TextStyle(
//                                       color: Colors.black,
//                                       fontSize: 14.0,
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               Container(
//                                 padding: EdgeInsets.only(left: 10),
//                                 child: FlatButton(
//                                   shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.circular(18.0),
//                                   ),
//                                   color: Color.fromRGBO(229, 230, 235, 1),
//                                   textColor: Colors.red,
//                                   padding: EdgeInsets.all(14.0),
//                                   onPressed: () {
//                                     Navigator.pushNamed(context, ALL_FRIEND);
//                                   },
//                                   child: Text(
//                                     "Tất cả bạn bè",
//                                     style: TextStyle(
//                                       color: Colors.black,
//                                       fontSize: 14.0,
//                                     ),
//                                   ),
//                                 ),
//                               )
//                             ],
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                   FriendContainer(getFriendResponse: getFriendResponse),
//                 ],
//               ),
//               Container(
//                 child: Center(
//                   child: Text('Mọi người'),
//                 ),
//                 color: Color.fromRGBO(201, 204, 209, 1),
//               ),
//               Container(
//                 child: Center(
//                   child: Text('Ảnh'),
//                 ),
//                 color: Color.fromRGBO(201, 204, 209, 1),
//               ),
//               Container(
//                 child: Center(
//                   child: Text('Video'),
//                 ),
//                 color: Color.fromRGBO(201, 204, 209, 1),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
