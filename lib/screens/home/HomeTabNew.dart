import 'package:facebook/api/friend/get_requested_friends.dart';
import 'package:facebook/api/listpost/get_list_posts.dart';
import 'package:facebook/api/payload/GetRequestFriendResponse.dart';
import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/friend/friendtab/FriendContainter.dart';
import 'package:facebook/screens/login/home_tab.dart';
import 'package:facebook/screens/notification/notificationDev.dart';
import 'package:facebook/screens/notification/notificationFakeData.dart';
import 'package:facebook/screens/notification/notificationScreen.dart';
import 'package:facebook/screens/post/AddPost.dart';
import 'package:facebook/screens/search/listpost/SearchFuture.dart';
import 'package:facebook/screens/video/VideoTab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class HomeTabNew extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Constants.homeState = TabBarDemo2();
    return Scaffold(
      body: Constants.homeState,
    );
  }
}

class TabBarDemo2 extends StatefulWidget {
  _TabBarDemoState1 s;
  @override
  _TabBarDemoState1 createState() {
    s = _TabBarDemoState1();
    return s;
  }
}

class _TabBarDemoState1 extends State<TabBarDemo2>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;
  Future<SearchResponse> searchResponse;
  String token;
  String authorization;
  String keywork;
  int user_id;
  int index;
  int count;
  TextEditingController searchField;
  SearchFuture searchFuture;
  Future<GetRequestFriendResponse> getFriendResponse;
  ScrollController _scrollViewController;

  SearchResponse listPostResponse;
  _TabBarDemoState1();

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bạn bè"),
    ),
    // Tab(
    //   child: Text("Mọi người"),
    // ),
    Tab(
      child: Text("Video"),
    ),
    Tab(
      child: Text("Thông báo"),
    ),
    Tab(
      child: Text("Menu")
    )
  ];
  bool isLoading = false;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  var random;

  Future _loadData() async {
    // perform fetching data delay
    print("@@@@@@@@@@@@@@@@@@@@@@@@@: pullup");
    var sa = await get_list_post(
        token, token, user_id, index, count, Constants.lastIdPost);
    setState(() {
      searchFuture.s.searchResponseS.data.addAll(sa.data);
      isLoading = false;
    });
    searchFuture.reload();
  }

  Future deleteData(SearchData data) {
    // perform fetching data delay
    print("@@@@@@@@@@@@@@@@@@@@@@@@@: deleteData");
    // List<SearchData> dataI = Constants.listPost.data;
    List<SearchData> dataI = searchFuture.s.searchResponseS.data;
    int i;
    SearchData deleteElement;
    for (i = 0; i < dataI.length; i++) {
      if (dataI[i].id == data.id) {
        deleteElement = dataI[i];
        print("#######################index: " + i.toString());
        break;
      }
    }
    setState(() {
      searchFuture.s.searchResponseS.data.removeAt(i);
    });
    searchFuture.reload();
  }

  Future refreshList() async {
    print("#######################: pulldown");
    Constants.lastIdPost = 100000;
    // var sa1 = await get_list_post(
    //     token, token, user_id, index, count, Constants.lastIdPost);
    // while (searchFuture.s.searchResponseS.data.length > sa1.data.length) {
    //   setState(() {
    //     searchFuture.s.searchResponseS.data.removeAt(0);
    //   });
    //   searchFuture.reload();
    // }

    var sa1 = await get_list_post(
        token, token, user_id, index, count, Constants.lastIdPost);
    // while (searchFuture.s.searchResponseS.data.length > sa1.data.length) {
    //   setState(() {
    //     searchFuture.s.searchResponseS.data.removeAt(0);
    //   });
    //   searchFuture.reload();
    // }

    // while (searchFuture.s.searchResponseS.data.length > sa1.data.length) {

    setState(() {
      int tmp = sa1.data.length;
      searchFuture.s.searchResponseS.data
          .removeRange(0, searchFuture.s.searchResponseS.data.length);
    });
    searchFuture.reload();
    // }
    Constants.lastIdPost = 100000;
    var sa = await get_list_post(
        token, token, user_id, index, count, Constants.lastIdPost);
    setState(() {
      searchFuture.s.searchResponseS.data.addAll(sa.data);
    });
    searchFuture.reload();
  }

  @override
  initState() {
    super.initState();
    authorization = Constants.authorization;
    token = Constants.token;
    user_id = Constants.user_id;
    searchResponse =
        get_list_post(token, token, user_id, index, count, Constants.lastIdPost)
            as Future<SearchResponse>;
    getFriendResponse = get_requested_friends(authorization, token, user_id)
        as Future<GetRequestFriendResponse>;
    searchFuture = SearchFuture(
      searchResponse: searchResponse,
    );
    _scrollViewController = new ScrollController();
    _controller = TabController(length: list.length, vsync: this);
    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new NestedScrollView(
        controller: _scrollViewController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            new SliverAppBar(
              backgroundColor: Colors.white,
              pinned: true,
              floating: true,
              forceElevated: innerBoxIsScrolled,
              title: Container(
                height: 85.0,
                decoration: BoxDecoration(
                  color: Colors.white,
                  // boxShadow: const [
                  //   BoxShadow(
                  //     color: Colors.black12,
                  //     offset: Offset(0, 2),
                  //     blurRadius: 4.0,
                  //   ),
                  // ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text(
                        'facebook',
                        style: const TextStyle(
                          color: Color(0xFF1777F2),
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          letterSpacing: -1.2,
                        ),
                      ),
                    ),
                    Container(
                      height: double.infinity,
                      width: 60.0,
                      child: null,
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            width: 40,
                            margin: const EdgeInsets.all(6.0),
                            decoration: BoxDecoration(
                              color: Colors.grey[200],
                              shape: BoxShape.circle,
                            ),
                            child: IconButton(
                              icon: Icon(Icons.search),
                              iconSize: 25.0,
                              color: Colors.black,
                              onPressed: () => Navigator.pushNamed(context, SEARCH_POST),
                            ),
                          ),
                          Container(
                            width: 40,
                            // margin: const EdgeInsets.all(6.0),
                            decoration: BoxDecoration(
                              color: Colors.grey[200],
                              shape: BoxShape.circle,
                            ),
                            child: IconButton(
                              icon: Icon(MdiIcons.facebookMessenger),
                              iconSize: 25.0,
                              color: Colors.black,
                              onPressed: () => Navigator.pushNamed(context, MESSENGER_MAIN),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              bottom: new PreferredSize(
                child: Container(
                  // padding: EdgeInsets.only(top: 20),
                  child: TabBar(
                    controller: _controller,
                    isScrollable: true,
                    unselectedLabelColor: Colors.grey,
                    labelColor: Colors.blue,
                    tabs: [
                      Container(
                        width: 40,
                        height: 50,
                        alignment: Alignment.center,
                        child: Tab(
                          child: Icon(
                            Icons.home_outlined,
                            // color: Color.fromRGBO(102, 102, 107, 1),
                          ),
                        ),
                      ),
                      Container(
                        width: 40,
                        height: 50,
                        alignment: Alignment.center,
                        child: Tab(
                            child: Icon(
                          MdiIcons.accountCircleOutline,
                        )),
                      ),
                      Container(
                        width: 40,
                        height: 50,
                        alignment: Alignment.center,
                        child: Tab(
                          child: Icon(Icons.ondemand_video),
                        ),
                      ),
                      Container(
                        width: 40,
                        height: 50,
                        alignment: Alignment.center,
                        child: Tab(
                          child: Icon(
                            MdiIcons.bellOutline,
                          ),
                        ),
                      ),
                      Container(
                        width: 40,
                        height: 50,
                        alignment: Alignment.center,
                        child: Tab(
                          child: Icon(
                            Icons.menu,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                preferredSize: Size.fromHeight(52.0),
              ),
            ),
          ];
        },
        body: new TabBarView(
          children: <Widget>[
            Container(
              child: RefreshIndicator(
                key: refreshKey,
                child: Container(
                  // child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.fromLTRB(0, 0, 10, 10),
                              color: Colors.white,
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        padding:
                                            EdgeInsets.fromLTRB(10, 5, 10, 0),
                                        child: CircleAvatar(
                                            radius: 30,
                                            backgroundImage:
                                                Constants.avatar == null
                                                    ? AssetImage(
                                                        "assets/ramdom.jpg")
                                                    : NetworkImage(
                                                        Constants.FileURI +
                                                            Constants.avatar)),
                                      ),
                                      Flexible(
                                        child: TextField(
                                          // textInputAction:
                                          //     TextInputAction.newline,
                                          readOnly: true,
                                          onTap: () {
                                            Navigator.pushNamed(
                                                context, ADD_POST);
                                          },
                                          style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.black),
                                          decoration: InputDecoration(
                                            prefixIcon: Icon(
                                              Icons.search,
                                              color: Colors.black,
                                            ),
                                            filled: true,
                                            fillColor: Color.fromRGBO(
                                                240, 242, 245, 1),
                                            hintText: 'Bạn đang nghĩ gì?',
                                            contentPadding:
                                                const EdgeInsets.only(
                                                    left: 8.0, top: 11.0),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white),
                                              borderRadius:
                                                  BorderRadius.circular(26.7),
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white),
                                              borderRadius:
                                                  BorderRadius.circular(26.7),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Divider(
                                    indent: 10,
                                    endIndent: 0,
                                    thickness: 1,
                                    color: Colors.grey,
                                  ),
                                  IntrinsicHeight(
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(left: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(
                                                Icons.video_call_outlined,
                                                color: Colors.red,
                                                size: 21,
                                              ),
                                              Container(
                                                child: Text(
                                                  "Phát trực tiếp",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Colors.grey),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        VerticalDivider(
                                          color: Colors.black,
                                          thickness: 1,
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: 4, right: 4),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(
                                                Icons.image,
                                                color: Colors.green,
                                                size: 21,
                                              ),
                                              InkWell(
                                                  onTap: () {
                                                    Navigator.pushNamed(
                                                        context, ADD_POST);
                                                  },
                                                  child: Container(
                                                    child: Text(
                                                      "Ảnh",
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: Colors.grey),
                                                    ),
                                                  ))
                                            ],
                                          ),
                                        ),
                                        VerticalDivider(
                                          color: Colors.black,
                                          thickness: 1,
                                        ),
                                        Container(
                                          child: Row(
                                            children: <Widget>[
                                              Icon(
                                                Icons.flag,
                                                color: Colors.red,
                                                size: 21,
                                              ),
                                              Container(
                                                child: Text(
                                                  "Sự kiện trong đời",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Colors.grey),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      searchFuture != null &&
                              searchFuture.s != null &&
                              searchFuture.s.searchResponseS != null &&
                              searchFuture.s.searchResponseS.data.isEmpty
                          ? Container(
                              height: 396,
                            )
                          : Container(),
                      //           Center(
                      //   child: CircularProgressIndicator(),
                      // ):Container(),
                      Expanded(
                        child: NotificationListener<ScrollNotification>(
                            onNotification: (ScrollNotification scrollInfo) {
                              if (!isLoading &&
                                  scrollInfo.metrics.pixels ==
                                      scrollInfo.metrics.maxScrollExtent) {
                                _loadData();
                                // start loading data
                                setState(() {
                                  isLoading = true;
                                });
                              }
                            },
                            child: searchFuture),
                      ),
                      Container(
                        height: isLoading ? 50.0 : 0,
                        color: Colors.transparent,
                        child: Center(
                          child: new CircularProgressIndicator(),
                        ),
                      ),
                    ],
                  ),
                  // ),
                  color: Color.fromRGBO(201, 204, 209, 1),
                ),
                onRefresh: refreshList,
              ),
            ),

            Column(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                  color: Color.fromRGBO(201, 204, 209, 1),
                  child: Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    color: Colors.white,
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                              child: Text(
                                "Bạn bè",
                                style: TextStyle(
                                    fontSize: 32, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Expanded(
                              child: Text(""),
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 0.0),
                                  child: RawMaterialButton(
                                    onPressed: () {
                                      Navigator.pushNamed(context, SEARCH_POST);
                                    },
                                    elevation: 2.0,
                                    fillColor: Color.fromRGBO(229, 230, 235, 1),
                                    child: Icon(
                                      Icons.search,
                                      // size: 5.0,
                                    ),
                                    padding: EdgeInsets.all(4.0),
                                    shape: CircleBorder(),
                                  )),
                            ),
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              child: FlatButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                ),
                                color: Color.fromRGBO(229, 230, 235, 1),
                                textColor: Colors.red,
                                padding: EdgeInsets.all(14.0),
                                onPressed: () {
                                  Navigator.pushNamed(context, SUGGEST_FRIEND);
                                },
                                child: Text(
                                  "Gợi ý",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.0,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              child: FlatButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                ),
                                color: Color.fromRGBO(229, 230, 235, 1),
                                textColor: Colors.red,
                                padding: EdgeInsets.all(14.0),
                                onPressed: () {
                                  Navigator.pushNamed(context, ALL_FRIEND);
                                },
                                child: Text(
                                  "Tất cả bạn bè",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.0,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                FriendContainer(getFriendResponse: getFriendResponse),
              ],
            ),
            VideoTab(),
            // Container(
            //   child: Center(
            //     child: Text('Mọi người'),
            //   ),
            //   color: Color.fromRGBO(201, 204, 209, 1),
            // ),
            Container(
              child: NotificationDev()
              // color: Color.fromRGBO(201, 204, 209, 1),
            ),
            MenuTab()
          ],
          controller: _controller,
        ),
      ),
    );
  }
}
