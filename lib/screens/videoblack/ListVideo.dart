import 'dart:ffi';

import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/screens/search/listpost/SinglePostSearch.dart';
import 'package:facebook/screens/videoblack/SingleBlackVideo.dart';
import 'package:flutter/material.dart';

class ListVideo extends StatefulWidget {
  final List<SearchData> data;
  ListVideo(@required this.data);
  Lax createState() => Lax(data);
}

class Lax extends State<ListVideo> {
  List<SearchData> data;

  @override
  void initState() {
    super.initState();

  }

  Lax(@required this.data);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: data.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: EdgeInsets.only(top: 0),
      itemBuilder: (BuildContext context, int index) {
        if (index < data.length){
          return SingleBlackVideo(data[index]);
        } else {
          return Padding(
                    padding: EdgeInsets.symmetric(vertical: 32.0),
                    child: Center(child: CircularProgressIndicator()),
                  );
        }
      },
    );
  }

}