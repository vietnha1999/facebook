import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/screens/search/listpost/ListPostSearch.dart';
import 'package:facebook/screens/videoblack/ListVideo.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class VideoBlackFuture extends StatefulWidget {
  Future<SearchResponse> searchResponse;
  VideoBlackFuture({@required this.searchResponse});
  _TabBarDemoState s;

  reload() {
    s.setState(() {});
  }

  @override
  _TabBarDemoState createState() {
    this.s = new _TabBarDemoState(this.searchResponse);
    return s;
  } 
}

class _TabBarDemoState extends State<VideoBlackFuture> {
  
    Future<SearchResponse> searchResponse;
    SearchResponse searchResponseS;
    _TabBarDemoState(@required this.searchResponse);

    @override
    Widget build(BuildContext context) {
      return Container(
        child:
        FutureBuilder<SearchResponse>(
          future: searchResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              print("___________________________________________________");
              if (searchResponseS==null) {
                searchResponseS = snapshot.data;
              }
              return ListVideo(searchResponseS.data);
            } else if (snapshot.hasError) {
              return Text("Loi");
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        )
      );
    }
}
  