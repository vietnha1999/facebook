
import 'package:facebook/api/friend/get_list_suggested_friends.dart';
import 'package:facebook/api/friend/get_user_friends.dart';
import 'package:facebook/api/listpost/get_list_black_video.dart';
import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/friend/allfriend/AllFriendContainer.dart';
import 'package:facebook/screens/friend/suggestfiend/SuggestFriendContainer.dart';
import 'package:facebook/screens/videoblack/VideoBlackFuture.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class VideoBlack extends StatefulWidget {
  
  int id;

  VideoBlack({this.id});

  @override
  _TabBarDemoState createState() => _TabBarDemoState(id: id);
}

class _TabBarDemoState extends State<VideoBlack>
    with SingleTickerProviderStateMixin {
  Future<GetFriendResponse> getFriendResponse;
  int user_id;
  String token;
  String authorization;
  int index;
  int count;
  VideoBlackFuture videoBlackFuture;
  Future<SearchResponse> videoResponse;

  int id;

  _TabBarDemoState({this.id});

  @override
  void initState() {
    // TODO: implement initState
    videoResponse = get_list_black_video(Constants.authorization, Constants.token, Constants.user_id, 0, 0, id)
        as Future<SearchResponse>;
    videoBlackFuture = VideoBlackFuture(searchResponse: videoResponse);
    authorization = Constants.authorization; 
    token = Constants.token; 
    user_id = Constants.user_id;
    getFriendResponse = get_list_suggested_friends(
        authorization,
        token, index, count) as Future<GetFriendResponse>;
    super.initState();
    // Create TabController for getting the index of current tab
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      backgroundColor: Colors.black,
      body: DefaultTabController(
        length: 1, // AddedAdded
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: new AppBar(
              actions: [
                IconButton(
                    color: Colors.white,
                    icon: Icon(Icons.search),
                    onPressed: () {
                      Navigator.pushNamed(context, SEARCH_POST);
                    }),
              ],
              elevation: 0,
              backgroundColor: Colors.black,
              title: new 
              Align(
                alignment: Alignment.center,
                child: Container(
                child: new Text(
                  'Watch',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16
                  ),
                ),
                padding: EdgeInsets.only(top: 0),
              ),
              ),
              
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.white,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {Navigator.pop(context);}))),
          body: new TabBarView(
            children: <Widget>[
              SingleChildScrollView(
                child: videoBlackFuture,
              )
            ],
          ),
        ),
      ),
    );
  
  }
}
