import 'package:facebook/api/comment/get_comment.dart';
import 'package:facebook/api/comment/set_comment.dart';
import 'package:facebook/api/like/like.dart';
import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:facebook/api/payload/GetRequestFriendResponse.dart';
import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/screens/comment/CommentFuture.dart';
import 'package:facebook/screens/comment/ListSingleComment.dart';
import 'package:facebook/screens/singlepost/singlepost.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class DataPass {
  int pass_liked;
  String pass_is_liked;
  int comment;
}

class Comment extends StatefulWidget {
  int liked;
  String is_liked;
  int comment;
  int id;
  Comment({this.liked, this.is_liked, this.comment, this.id});

  @override
  _TabBarDemoState createState() => _TabBarDemoState(liked, is_liked, comment, id);
}

class _TabBarDemoState extends State<Comment>
    with SingleTickerProviderStateMixin {
  int liked;
  String is_liked;
  int comment;
  int sizeComent;
  int id;

  TextEditingController searchField;
  CommentFuture commentFuture;

  _TabBarDemoState(this.liked, this.is_liked, this.comment, this.id);
  Future<GetCommentResponse> getCommentResponse;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    searchField = new TextEditingController();
    sizeComent = 0;
    getCommentResponse = get_comment(Constants.authorization, id, 0, 0);
    commentFuture = CommentFuture(
                          getCommentResponse: getCommentResponse,
                        );
    // Create TabController for getting the index of current tab
  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      direction: DismissDirection.vertical,
      key: const Key('key'),
      onDismissed: (_) {
        DataPass data = DataPass();
        data.pass_liked = liked;
        data.pass_is_liked = is_liked;
        data.comment = comment;
        Navigator.of(context, rootNavigator: true).pop(data);
      },
      child: Scaffold(
        body: DefaultTabController(
          length: 1, // AddedAdded
          child: Scaffold(
            appBar: new AppBar(
              bottom: PreferredSize(
                  child: Container(
                    color: Color.fromRGBO(211, 211, 211, 1),
                    height: 1.0,
                  ),
                  preferredSize: Size.fromHeight(1.0)),
              actions: [
                InkWell(
                    onTap: () {
                      setState(() {
                        is_liked == "false"
                            ? liked = liked + 1
                            : liked = liked - 1;
                        is_liked == "false"
                            ? is_liked = "true"
                            : is_liked = "false";
                        like(Constants.authorization, Constants.token,
                                  id);
                      });
                    },
                    child: Row(
                      children: [
                        is_liked == "false"
                            ? Container(
                          padding:EdgeInsets.only(right: 12),
                          child: Icon(
                            MdiIcons.thumbUpOutline,
                            size: 26,
                            color: Color.fromRGBO(108, 108, 108, 1),
                          ),
                        )
                            : Container(
                          padding:EdgeInsets.only(right: 12),
                          child: Icon(
                            MdiIcons.thumbUp,
                            size: 26,
                            color: Color.fromRGBO(23, 112, 227, 1),
                          ),
                        )
                      ],
                    ))
              ],
              elevation: 0,
              backgroundColor: Colors.white,
              title: new Container(
                child: Row(
                  children: [
                    Text(
                      liked.toString(),
                      style: TextStyle(color: Colors.black),
                    ),
                    Icon(
                      Icons.chevron_right_sharp,
                      color: Colors.black,
                    ),
                  ],
                ),
                padding: EdgeInsets.only(top: 0),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: Row(
                    children: [
                      CircleAvatar(
                        radius: 13,
                        backgroundImage: AssetImage("assets/Icon/17.jpg"),
                      ),
                    ],
                  )),
            ),
            backgroundColor: Colors.white,
            body: new TabBarView(
              children: <Widget>[
                SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top:10,left: 10, bottom: 14),
                        child: Text("Xem các bình luận trước ...",style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500, color:Colors.black
                        ),),
                      ),
                      commentFuture,

                    ],

                  ),
                ),
              ],
            ),
            bottomNavigationBar:
                
            Row(
              children: [

                Container(
                  padding: EdgeInsets.only(right: 18,bottom: 6),
                  child:  CircleAvatar(
                    backgroundImage: AssetImage("assets/Icon/18.png"),
                    backgroundColor: Colors.white,
                    radius: 17,
                  ),
                ),


                Flexible(
                  child: TextField(

                    // textInputAction: TextInputAction.newline,
                    textInputAction: TextInputAction.newline,
                    style: TextStyle(fontSize: 16.0, color: Colors.black),
                    controller: searchField,
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    onChanged: (String value) {
                      setState(() {
                        // searchField.text = value;
                      });
                    },
                    decoration: InputDecoration.collapsed(
                        hintText: 'Viết bình luận...',
                        hintStyle: TextStyle(color: Colors.black)),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 7,right: 7, bottom: 6),
                  child:  CircleAvatar(
                    backgroundImage: AssetImage("assets/Icon/20.png"),
                    backgroundColor: Colors.white,
                    radius: 16,
                  ),
                ),
                searchField.text.length==0?
                Container(
                  padding: EdgeInsets.only(left: 7,right: 7, bottom: 6),
                  child:  CircleAvatar(
                    backgroundImage: AssetImage("assets/Icon/19.png"),
                    backgroundColor: Colors.white,
                    radius: 16,
                  ),
                ):
                  InkWell(
                    onTap: () async {
                      GetCommentResponse getC = await get_comment(Constants.authorization, id, 0, 0);
                      SetCommentResponse setC = await set_comment(Constants.authorization, Constants.token, id, searchField.text);
                      getC.data.add(setC.data);
                      setState(() {
                        commentFuture.s.getCommentResponseS = getC;
                      });
                      commentFuture.reload();
                      comment++;
                      searchField.text = "";
                      FocusScope.of(context).previousFocus();
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: 7,right: 7,bottom:6),
                      child:  CircleAvatar(
                        backgroundImage: AssetImage("assets/Icon/19.png"),
                        backgroundColor: Colors.blue,
                        radius: 16,
                      ),
                    ),
                  ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}

// ignore: non_constant_identifier_names
Widget SingleComment() {
  return Container(
    child: Row(
      children: [
        CircleAvatar(
          radius: 20,
          backgroundImage: NetworkImage(Constants.FileURI + Constants.avatar),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(Constants.userName),
                  Text("Ha ha vui phết"),
                ],
              ),
            ),
            RichText(
              text: TextSpan(children: [
                TextSpan(text: "1 ngày", style: TextStyle(color: Colors.black)),
                TextSpan(
                    text: "   Thích", style: TextStyle(color: Colors.black)),
                TextSpan(
                    text: "   Trả lời", style: TextStyle(color: Colors.black)),
              ]),
            )
          ],
        )
      ],
    ),
  );
}

Future<DataPass> showComment(BuildContext context, int liked, String is_liked, int comment, int id) async {
  DataPass data = await showDialog(
    context: context,
    builder: (BuildContext context) {
      return Comment(liked: liked, is_liked: is_liked, comment: comment, id: id);
    },
  );
  return data;
}

