import 'package:facebook/api/comment/get_comment.dart';
import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/screens/comment/ListSingleComment.dart';
import 'package:facebook/screens/search/listpost/ListPostSearch.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

class CommentFuture extends StatefulWidget {
  Future<GetCommentResponse> getCommentResponse;
  CommentFuture({@required this.getCommentResponse});
  _TabBarDemoState s;

  reload() {
    s.setState(() {});
  }

  @override
  _TabBarDemoState createState() {
    this.s = new _TabBarDemoState(this.getCommentResponse);
    return s;
  }
}

class _TabBarDemoState extends State<CommentFuture> {
  Future<GetCommentResponse> getCommentResponse;
  GetCommentResponse getCommentResponseS;

  _TabBarDemoState(@required this.getCommentResponse);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: getCommentResponseS != null
            ? ListSingleComment(getCommentResponseS.data)
            : getCommentResponseS == null
                ? FutureBuilder<GetCommentResponse>(
                    future: getCommentResponse,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListSingleComment(snapshot.data.data);
                      } else if (snapshot.hasError) {
                        return Text("Loi");
                      }
                      // By default, show a loading spinner.
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ));
  }
}
