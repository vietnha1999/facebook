import 'package:facebook/api/payload/GetUserInfoData.dart';
import 'package:facebook/api/payload/GetUserInfoResponse.dart';
import 'package:facebook/api/user/get_user_info.dart';
import 'package:facebook/router/Router.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class Personal extends StatefulWidget {
  final int user_id;

  const Personal({this.user_id});

  @override
  _AnotherProfileState createState() => _AnotherProfileState(user_id);
}

class _AnotherProfileState extends State<Personal> {
  String token;
  String authorization;
  String _fullName = " Pham Linh Chi";
  String _status = "Hoa oải hương";
  String _bio = "\"Em đã làm gì có người yêu? Em còn đang sợ ế đây này\"";
  String _followers = "173";
  String _posts = "24";
  String _scores = "450";
  int stateAccept = 1;

  int user_id;
  Future<GetUserInfoResponse> getUserInfoResponse;

  _AnotherProfileState(this.user_id);

  @override
  initState() {
    super.initState();
    authorization = Constants.authorization;
    token = Constants.token;
    print("###############################");
    print(user_id);
    getUserInfoResponse = get_user_info(authorization, token, user_id)
        as Future<GetUserInfoResponse>;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            title: new Container(
              child: new InkWell(
                onTap: () {
                  Navigator.pushNamed(context, SEARCH_POST);
                },
                child: TextField(
                  // controller: searchField,
                  textAlign: TextAlign.left,
                  // autofocus: true,
                  style: TextStyle(fontSize: 22.0, color: Colors.black),
                  // onSubmitted: searchEvent(searchField.text),
                  // onSubmitted: searchEvent(searchField.text),
                  onSubmitted: (String value) async {
                    // var sa = await search(token, token, value, user_id, index, count);
                    // setState(() => searchFuture.s.searchResponseS = sa
                    //     );
                    // searchFuture.reload();
                  },
                  textInputAction: TextInputAction.search,
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    // suffixIcon: (searchField.text.length > 0)?
                    suffixIcon: IconButton(
                        color: Colors.black,
                        icon: Icon(Icons.close),
                        onPressed: () {
                          // searchField.text = '';
                        }),
                    // : null,
                    filled: true,
                    fillColor: Color.fromRGBO(240, 242, 245, 1),
                    hintText: 'Tìm kiếm',
                    contentPadding: const EdgeInsets.only(left: 8.0, top: 11.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                  ),
                ),
              ),
              padding: EdgeInsets.only(top: 8, bottom: 8),
            ),
            leading: Padding(
                padding: EdgeInsets.only(left: 12),
                child: IconButton(
                    color: Colors.black,
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    }))),
        body: FutureBuilder<GetUserInfoResponse>(
          future: getUserInfoResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              GetUserInfoData user = snapshot.data.data;
              return ProfilePage(context, user.username, _status, _bio,
                  _followers, _posts, _scores, user.avatar, user.is_friend);
            } else if (snapshot.hasError) {
              return Text("Loi");
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ));
  }

  Widget _buildProfileImage(avatar) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(Constants.fakeImage), fit: BoxFit.cover)),
      child: Container(
        width: double.infinity,
        height: 240,
        child: Container(
          alignment: Alignment(0.0, 2.5),
          child: CircleAvatar(
            backgroundImage: avatar != null
                ? NetworkImage(Constants.FileURI + avatar)
                : NetworkImage(
                    'https://1.bp.blogspot.com/-m3UYn4_PEms/Xnch6mOTHJI/AAAAAAAAZkE/GuepXW9p7MA6l81zSCnmNaFFhfQASQhowCLcBGAsYHQ/s1600/Cach-Lam-Avatar-Dang-Hot%2B%25281%2529.jpg'),
            radius: 80.0,
          ),
        ),
      ),
    );
  }

  Widget _buildFullName(_fullName) {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      _fullName,
      style: _nameTextStyle,
    );
  }

  Widget _buildStatus(BuildContext context, _status) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Text(
        _status,
        style: TextStyle(
          fontFamily: 'Spectral',
          color: Colors.black,
          fontSize: 20.0,
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }

  Widget _buildStatItem(String label, String count) {
    TextStyle _statLabelTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 16.0,
      fontWeight: FontWeight.w200,
    );

    TextStyle _statCountTextStyle = TextStyle(
      color: Colors.black54,
      fontSize: 24.0,
      fontWeight: FontWeight.bold,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          count,
          style: _statCountTextStyle,
        ),
        Text(
          label,
          style: _statLabelTextStyle,
        ),
      ],
    );
  }

  Widget _buildStatContainer(_followers, _posts, _scores) {
    return Container(
      height: 60.0,
      margin: EdgeInsets.only(top: 8.0),
      decoration: BoxDecoration(
        color: Color(0xFFEFF4F7),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildStatItem("Followers", _followers),
          _buildStatItem("Posts", _posts),
          _buildStatItem("Likes", _scores),
        ],
      ),
    );
  }

  Widget _buildBio(BuildContext context, _bio) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'Spectral',
      fontWeight: FontWeight.w400,
      //try changing weight to w500 if not thin
      fontStyle: FontStyle.italic,
      color: Color(0xFF799497),
      fontSize: 16.0,
    );

    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.all(8.0),
      child: Text(
        _bio,
        textAlign: TextAlign.center,
        style: bioTextStyle,
      ),
    );
  }

  Widget _buildSeparator(Size screenSize) {
    return Container(
      width: screenSize.width / 1.6,
      height: 2.0,
      color: Colors.black54,
      margin: EdgeInsets.only(top: 4.0),
    );
  }

  Widget _buildGetInTouch(BuildContext context, _fullName) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.only(top: 8.0),
      child: Text(
        "Vào trang của tui làm gì đấy? ${_fullName.split(" ")[0]},",
        style: TextStyle(fontFamily: 'Roboto', fontSize: 16.0),
      ),
    );
  }

  Widget _buildButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () => print("followed"),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
                  border: Border.all(),
                  color: Colors.blue,
                ),
                child: Center(
                  child: Text(
                    "FOLLOW",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(width: 10.0),
          Expanded(
            child: InkWell(
              onTap: () => print("Message"),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
                  border: Border.all(),
                ),
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text(
                      "MESSAGE",
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showMessages() {
    Size size = MediaQuery.of(context).size;
    return new Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20, top: 10, bottom: 10, right: 10),
          height: 60,
          width: size.width * 0.6,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.0),
                side: BorderSide(color: Color.fromRGBO(23, 119, 242, 1))),
            color: Color.fromRGBO(23, 119, 242, 1),
            onPressed: () {},
            child: Container(
              padding: EdgeInsets.only(left: 25),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.message,
                    size: 24,
                    color: Colors.white,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Text(
                      "Nhắn tin",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 10, bottom: 10, right: 7, left: 7),
          height: 60,
          width: size.width * 0.18,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.0),
                side: BorderSide(
                  color: Color.fromRGBO(229, 230, 235, 1),
                )),
            onPressed: () {},
            color: Color.fromRGBO(229, 230, 235, 1),
            child: Icon(
              Icons.person_remove,
              size: 26,
            ),
          ),
        ),
        Container(
            padding: EdgeInsets.only(top: 10, bottom: 10, left: 7),
            height: 60,
            width: size.width * 0.18,
            child: settingPersonal())
      ],
    );
  }

  Widget settingPersonal() {
    return RaisedButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6.0),
          side: BorderSide(
            color: Color.fromRGBO(229, 230, 235, 1),
          )),
      onPressed: () {
        Navigator.pushNamed(context, SETTING_PERSONAL);
      },
      color: Color.fromRGBO(229, 230, 235, 1),
      child: Icon(
        Icons.more_horiz,
        size: 26,
      ),
    );
  }

  Widget showAddFriends() {
    Size size = MediaQuery.of(context).size;
    return new Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20, top: 10, bottom: 10, right: 10),
          height: 60,
          width: size.width * 0.6,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.0),
                side: BorderSide(color: Color.fromRGBO(23, 119, 242, 1))),
            color: Color.fromRGBO(23, 119, 242, 1),
            onPressed: () {
              addFriendButton();
            },
            child: Container(
              padding: EdgeInsets.only(left: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.person_add,
                    size: 24,
                    color: Colors.white,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Text(
                      "Thêm bạn bè",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 10, bottom: 10, right: 7, left: 7),
          height: 60,
          width: size.width * 0.18,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.0),
                side: BorderSide(
                  color: Color.fromRGBO(229, 230, 235, 1),
                )),
            onPressed: () {},
            color: Color.fromRGBO(229, 230, 235, 1),
            child: Icon(
              Icons.message,
              size: 26,
            ),
          ),
        ),
        Container(
            padding: EdgeInsets.only(top: 10, bottom: 10, left: 7),
            height: 60,
            width: size.width * 0.18,
            child: settingPersonal())
      ],
    );
  }

  addFriendButton() {}

  Widget showAnser() {
    Size size = MediaQuery.of(context).size;
    return new Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 15, top: 10, bottom: 10, right: 10),
          height: 60,
          width: size.width * 0.78,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.0),
                side: BorderSide(color: Color.fromRGBO(23, 119, 242, 1))),
            color: Color.fromRGBO(23, 119, 242, 1),
            onPressed: () {},
            child: Container(
              padding: EdgeInsets.only(left: 70),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.group,
                    size: 24,
                    color: Colors.white,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Text(
                      "Trả lời",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
            padding: EdgeInsets.only(top: 10, bottom: 10, left: 7),
            height: 60,
            width: size.width * 0.18,
            child: settingPersonal()),
      ],
    );
  }

  Widget showInfomationUser() {
    return new Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 15, top: 15, bottom: 10, right: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.work,
                    size: 24,
                    color: Colors.black45,
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 4, left: 15),
                    child: Text(
                      "Đã làm việc tại",
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    ),
                  ),
                  Container(
                    child: Text(
                      "GameCenter vv",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 15, top: 15, bottom: 10, right: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.work,
                    size: 24,
                    color: Colors.black45,
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 4, left: 15),
                    child: Text(
                      "Đã làm việc tại",
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    ),
                  ),
                  Container(
                    child: Text(
                      "GameCenter vv",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 15, top: 15, bottom: 10, right: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.work,
                    size: 24,
                    color: Colors.black45,
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 4, left: 15),
                    child: Text(
                      "Đã làm việc tại",
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    ),
                  ),
                  Container(
                    child: Text(
                      "GameCenter vv",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 15, top: 15, bottom: 10, right: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.work,
                    size: 24,
                    color: Colors.black45,
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 4, left: 15),
                    child: Text(
                      "Đã làm việc tại",
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    ),
                  ),
                  Container(
                    child: Text(
                      "GameCenter vv",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            )
          ],
        )
      ],
    );
  }

  Widget ProfilePage(context, _fullName, _status, _bio, _followers, _posts,
      _scores, avatar, is_friend) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  _buildProfileImage(avatar),
                  SizedBox(height: 65),
                  _buildFullName(_fullName),
                  is_friend == 'true'
                      ? showMessages()
                      : is_friend == 'false'
                          ? showAddFriends()
                          : showAnser(),
                  showInfomationUser()
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
