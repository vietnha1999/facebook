import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/api/search/Search.dart';
import 'package:facebook/screens/search/listpost/SearchFuture.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class SettingPersonal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarDemo(),
    );
  }
}

class TabBarDemo extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<TabBarDemo>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;
  Future<SearchResponse> searchResponse;
  String token;
  String authorization;
  String keywork;
  int user_id;
  int index;
  int count;
  TextEditingController searchField;
  SearchFuture searchFuture;

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bài viết"),
    ),
    Tab(
      child: Text("Mọi người"),
    ),
    Tab(
      child: Text("Ảnh"),
    ),
    Tab(
      child: Text("Video"),
    ),
  ];

  refresh() {
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    searchField = TextEditingController();
    token = Constants.token;
    authorization = Constants.token;
    // keywork = "moi";
    user_id = Constants.user_id;
    index = 0;
    count = 0;
    searchFuture = SearchFuture(searchResponse: searchResponse);
    // searchResponse = search(token, token, keywork, user_id, index, count)
    //     as Future<SearchResponse>;
    // Create TabController for getting the index of current tab

    // searchField.addListener(() {
    //   setState(() {
    //   });
    // });
    _controller = TabController(length: list.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  void dispose() {
    searchField.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 1, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              title: new Container(
                child: new Text(
                  'Cài đặt trang cá nhân',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
                padding: EdgeInsets.only(top: 0),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigator.pop(context);
                      }))),
          body: new TabBarView(
            children: <Widget>[
              Container(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              child: Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 0, 16),
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Color.fromRGBO(
                                                201, 204, 209, 1)))),
                                child: Row(
                                  children: [
                                    Image.network(
                                      Constants.fakeImage,
                                      width: 20,
                                    ),
                                    Text(
                                      'Tìm hỗ trợ hoặc báo cáo trang cá nhân',
                                      style: TextStyle(
                                        fontSize: 16,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                            ),
                            Container(
                              child: Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Color.fromRGBO(
                                                201, 204, 209, 1)))),
                                child: InkWell(
                                  onTap: () {
                                    showAlertDialog(context);
                                  },
                                  child: Row(
                                    children: [
                                      Image.network(
                                        Constants.fakeImage,
                                        width: 20,
                                      ),
                                      Text(
                                        'Chặn',
                                        style: TextStyle(
                                          fontSize: 16,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                            ),
                            Container(
                              child: Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: Row(
                                  children: [
                                    Image.network(
                                      Constants.fakeImage,
                                      width: 20,
                                    ),
                                    Text(
                                      'Tìm kiếm trên trang cá nhân',
                                      style: TextStyle(
                                        fontSize: 16,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          // borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                        ),
                      ),
                      // Center(
                      //   child: Text('Tất cả'),
                      // ),
                      color: Color.fromRGBO(201, 204, 209, 1),
                    ),
                    Container(
                      child: Container(
                        padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              "Liên kết đến trang các nhân của Do Thu Huong",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              "Liên kết riêng của Nguyễn Văn A trên Facebook.",
                            ),
                            Container(
                                child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("https://sjmkdjsad",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14)),
                                FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  color: Color.fromRGBO(229, 230, 235, 1),
                                  textColor: Colors.red,
                                  // padding: EdgeInsets.all(14.0),
                                  onPressed: () {},
                                  child: Text(
                                    "Sao chép liên kết",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14.0,
                                    ),
                                  ),
                                ),
                              ],
                            ))
                          ],
                        ),
                        decoration: BoxDecoration(
                          // borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                        ),
                      ),
                      // Center(
                      //   child: Text('Tất cả'),
                      // ),
                      color: Color.fromRGBO(201, 204, 209, 1),
                    ),
                  ],
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              )
            ],
          ),
        ),
      ),
    );
  }
}
sheet() {
  return AlertDialog(
      title: Text('Bạn có chắc chắn muốn chặn Choi Loi không?'),
      content: Text(
          'Choi Loi sẽ không thể: \n   Xem bài viết của bạn \n   Gắn thẻ bạn \n   Mời bạn tham gia sự kiện hoặc nhóm \n   Trò chuyện với bạn \n    Thêm bạn làm bạn bè \n    Nếu các bạn là bạn bè, việc chặn Choi Loi cũng sẽ hủy kết bạn với anh ấy, cô ấy. \n Nếu bạn chỉ muốn giới hạn nội dung mình chia sẻ với Choi hoặc ẩn bớt nội dung về anh ấy trên Facebook, bạn có thẻ giảm tương tác với anh ấy.'),
      actions: [
        InkWell(
          onTap: () {},
          child: Text('Hủy'),
        ),
        InkWell(
          onTap: () {},
          child: Text('Chặn'),
        )
      ],
    );
}
// class Sheet extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return AlertDialog(
//       title: Text('Bạn có chắc chắn muốn chặn Choi Loi không?'),
//       content: Text(
//           'Choi Loi sẽ không thể: \n   Xem bài viết của bạn \n   Gắn thẻ bạn \n   Mời bạn tham gia sự kiện hoặc nhóm \n   Trò chuyện với bạn \n    Thêm bạn làm bạn bè \n    Nếu các bạn là bạn bè, việc chặn Choi Loi cũng sẽ hủy kết bạn với anh ấy, cô ấy. \n Nếu bạn chỉ muốn giới hạn nội dung mình chia sẻ với Choi hoặc ẩn bớt nội dung về anh ấy trên Facebook, bạn có thẻ giảm tương tác với anh ấy.'),
//       actions: [
//         InkWell(
//           onTap: () {},
//           child: Text('Hủy'),
//         ),
//         InkWell(
//           onTap: () {},
//           child: Text('Chặn'),
//         )
//       ],
//     );
//   }
// }
showAlertDialog(BuildContext context) {

  // set up the buttons
  Widget cancelButton = FlatButton(
    child: Text("Hủy", style: TextStyle(color: Colors.grey,),),
    onPressed:  () {
      Navigator.pop(context);
    },
    
  );
  Widget continueButton = FlatButton(
    child: Text("Chặn"),
    onPressed:  () {},
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text('Bạn có chắc chắn muốn chặn Choi Loi không?'),
      content: Text(
          'Choi Loi sẽ không thể: \n   Xem bài viết của bạn \n   Gắn thẻ bạn \n   Mời bạn tham gia sự kiện hoặc nhóm \n   Trò chuyện với bạn \n   Thêm bạn làm bạn bè \nNếu các bạn là bạn bè, việc chặn Choi Loi cũng sẽ hủy kết bạn với anh ấy, cô ấy. \nNếu bạn chỉ muốn giới hạn nội dung mình chia sẻ với Choi hoặc ẩn bớt nội dung về anh ấy trên Facebook, bạn có thẻ giảm tương tác với anh ấy.',
          style: TextStyle(
            fontSize: 16,
          ),
          ),
    actions: [
      cancelButton,
      continueButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}