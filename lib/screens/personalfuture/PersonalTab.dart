import 'package:facebook/api/listpost/get_list_video_posts.dart';
import 'package:facebook/api/listpost/get_list_video_posts.dart';
import 'package:facebook/api/payload/GetRequestFriendResponse.dart';
import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/api/personal/get_post_personal.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/personalfuture/PersonalFuture.dart';
import 'package:facebook/screens/video/VideoFuture.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class PersonalTab extends StatefulWidget {

  final int user_id;
  _TabVideoTab s;
  PersonalTab({this.user_id});
  @override
  _TabVideoTab createState() {
    s = _TabVideoTab(user_id);
    return s;
  }
}

class _TabVideoTab extends State<PersonalTab> {
  Future<SearchResponse> searchResponse;
  String token;
  String authorization;
  String keywork;
  int user_id;
  int index;
  int count;
  TextEditingController searchField;
  PersonalFuture searchFuture;
  Future<GetRequestFriendResponse> getFriendResponse;
  ScrollController _scrollViewController;

  SearchResponse listPostResponse;

  bool isLoading = false;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  var random;

  _TabVideoTab(this.user_id);

  Future _loadData() async {
    // perform fetching data delay
    print("@@@@@@@@@@@@@@@@@@@@@@@@@: pullup");
    var sa = await get_post_personal(
        Constants.token, Constants.token, user_id, index, count);
    setState(() {
      searchFuture.s.searchResponseS.data.addAll(sa.data);
      isLoading = false;
    });
    searchFuture.reload();
  }

  Future deleteData(SearchData data) {
    // perform fetching data delay
    print("@@@@@@@@@@@@@@@@@@@@@@@@@: deleteData");
    // List<SearchData> dataI = Constants.listPost.data;
    List<SearchData> dataI = searchFuture.s.searchResponseS.data;
    int i;
    SearchData deleteElement;
    for (i = 0; i < dataI.length; i++) {
      if (dataI[i].id == data.id) {
        deleteElement = dataI[i];
        print("#######################index: " + i.toString());
        break;
      }
    }
    setState(() {
      searchFuture.s.searchResponseS.data.removeAt(i);
    });
    searchFuture.reload();
  }

  Future refreshList() async {
    print("#######################: pulldown");
    refreshKey.currentState?.show(atTop: false);
    Constants.lastVideoIdPost = 100000;
    var sa = await get_post_personal(
        Constants.token, Constants.token, user_id, index, count);
    setState(() {
      searchFuture.s.searchResponseS.data.addAll(sa.data);
    });
    searchFuture.reload();
    Constants.lastVideoIdPost = 100000;
    var sa1 = await get_list_video_post(
        token, token, user_id, index, count, Constants.lastVideoIdPost);
    while (searchFuture.s.searchResponseS.data.length > sa1.data.length) {
      setState(() {
        searchFuture.s.searchResponseS.data.removeAt(0);
      });
      searchFuture.reload();
    }
  }

  @override
  initState() {
    super.initState();
    authorization = Constants.authorization;
    token = Constants.token;
    searchResponse = get_post_personal(
        Constants.token, Constants.token, user_id, index, count)
        as Future<SearchResponse>;
    _scrollViewController = new ScrollController();
    searchFuture = PersonalFuture(
      searchResponse: searchResponse,
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: RefreshIndicator(
        key: refreshKey,
        child: Container(
          // child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                child: NotificationListener<ScrollNotification>(
                    onNotification: (ScrollNotification scrollInfo) {
                      if (!isLoading &&
                          scrollInfo.metrics.pixels ==
        scrollInfo.metrics.maxScrollExtent) {
                        _loadData();
                        // start loading data
                        setState(() {
                          isLoading = true;
                        });
                      }
                    },
                    child: searchFuture),
              ),
              Container(
                height: isLoading ? 50.0 : 0,
                color: Colors.transparent,
                child: Center(
                  child: new CircularProgressIndicator(),
                ),
              ),
            ],
          ),
          // ),
          color: Color.fromRGBO(201, 204, 209, 1),
        ),
        onRefresh: refreshList,
      ),
    );
  }
}