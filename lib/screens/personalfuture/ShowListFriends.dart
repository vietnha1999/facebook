import 'dart:math';

import 'package:facebook/api/friend/get_user_friends.dart';
import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/search/listpost/ListPostSearch.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class ShowListFriends extends StatefulWidget {
  int user_id;
  ShowListFriends({@required this.user_id});
  _TabBarDemoState s;

  reload() {
    s.setState(() {});
  }

  @override
  _TabBarDemoState createState() {
    this.s = new _TabBarDemoState(this.user_id);
    return s;
  }
}

class _TabBarDemoState extends State<ShowListFriends> {
  int user_id;
  _TabBarDemoState(this.user_id);
  int numberListFriends = 100;
  Future<GetFriendResponse> getFriendResponse;

  @override
  initState() {
    super.initState();
    getFriendResponse = get_user_friends(
            Constants.authorization, Constants.token, user_id, 0, 0)
        as Future<GetFriendResponse>;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder<GetFriendResponse>(
        future: getFriendResponse,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            GetFriendResponse data = snapshot.data;
            return Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        "Bạn bè",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: size.width * 0.42),
                      child: Text(
                        " Tìm Bạn bè",
                        style: TextStyle(fontSize: 22, color: Colors.blue),
                      ),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        data.data.friends.length.toString() + " người bạn",
                        style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.w400,
                            color: Colors.black45),
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, right: 10.0),
                  width: MediaQuery.of(context).size.width - 10.0,
                  height: data.data.friends.length <= 3? MediaQuery.of(context).size.height * 0.48 *  1/2: MediaQuery.of(context).size.height * 0.48 *  1,
                  child: GridView.count(
                    crossAxisCount: 3,
                    childAspectRatio: 0.7,
                    padding:
                        EdgeInsets.only(top: 4, left: 2, bottom: 4, right: 2),
                    mainAxisSpacing: 2,
                    crossAxisSpacing: 3,
                    children: <Widget>[
                      for (int i=0;i<min(data.data.friends.length, 6);i++)
                      InkWell(
                        onTap: (){
                          Navigator.pushNamed(
                                          context, PERSONAL_PAGE,
                                          arguments: {"id": data.data.friends[i].id});
                                    }
                        ,
                        child: _singleFriend(Constants.FileURI + data.data.friends[i].avatar, data.data.friends[i].username),

                      )
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          left: 20, top: 10, bottom: 10, right: 20),
                      height: 64,
                      width: size.width,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        color: Color.fromRGBO(230, 235, 242, 1),
                        onPressed: () {
                          // addFriendButton();
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: size.width * 0.2),
                          child: Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(left: 5),
                                child: Text(
                                  "Xem tất cả bạn bè",
                                  style: TextStyle(
                                      fontSize: 19,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            );
          } else if (snapshot.hasError) {
            return Text("Loi");
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget _singleFriend(String img, String name) {
    return new Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 100.0,
            width: 100.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                image:
                    DecorationImage(image: NetworkImage(img), fit: BoxFit.fill)),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, left: 10, right: 10),
            child: Text(
              "${name}",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Colors.black),
            ),
          )
        ],
      ),
    );
  }
}
