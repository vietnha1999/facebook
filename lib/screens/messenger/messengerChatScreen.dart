import 'dart:developer';

import 'package:facebook/actions/userInfoAction.dart';
import 'package:facebook/screens/messenger/localComponents/messengerChatBottomBar.dart';
import 'package:facebook/screens/messenger/localComponents/messengerChatList.dart';
import 'package:facebook/screens/messenger/localComponents/messengerChatTopBar.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MessengerChatScreen extends StatelessWidget {
  UserInfoState zfriend;
  MessengerChatScreen({@required this.zfriend});
  @override
  Widget build(BuildContext context) {
    log("friendchat" + zfriend.id);
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                MessengerChatTopBar(),
                Divider(color: Colors.black38, thickness: 0.6, height: 0),
                MessengerChatList(),
                MessengerChatBottomBar()
              ],
            )
          )
        ]
      )
    );
  }
  
}