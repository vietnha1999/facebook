import 'dart:developer';

import 'package:facebook/screens/messenger/localComponents/messengerNewMessageList.dart';
import 'package:facebook/screens/messenger/localComponents/messengerTopBarOnlyTitle.dart';
import 'package:flutter/material.dart';

class MessengerNewMessageScreen extends StatelessWidget {
  final List<Map<String,String>> data;
  MessengerNewMessageScreen({@required this.data});
  @override
  Widget build(BuildContext context) {
    log("Length" + data.length.toString());
    return Expanded(
      child: Column(
        children: [
          MessengerTopBarOnlyTitle(title: "Tin nhắn mới"),
          MessengerNewMessageList(data: data)
        ],
      )
    );
  }
}