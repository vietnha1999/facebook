import 'dart:developer';
import 'package:camera/camera.dart';
import 'package:facebook/screens/messenger/messengerCameraDisplayPictureScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class MessengerCameraScreen extends StatefulWidget {
  final List<CameraDescription> camera;
  const MessengerCameraScreen({
    Key key,
    @required this.camera
  }) : super(key: key);

  @override
  _MessengerCameraState createState() => _MessengerCameraState();
}
class _MessengerCameraState extends State<MessengerCameraScreen> {
  CameraController _controller;
  int _cameraSelectedIdx;
  Future<void> _initializeControllerFuture;
  @override
  void initState() {
    super.initState();
    _initCamera(cameraSelectedIdx: 0);
  }
  
  @override
  Widget build(BuildContext context) {
    return 
      Scaffold(
        body: Stack(
          children: [
            FutureBuilder<void>(
              future: _initializeControllerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  // If the Future is complete, display the preview.
                  return AspectRatio(
                    aspectRatio: _controller.value.aspectRatio,
                    child: CameraPreview(_controller)
                  );
                } else {
                  // Otherwise, display a loading indicator.
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
            Align(
              alignment: Alignment.topCenter,
              child: InkWell(
                onTap: _onSwitchCamera,
                child: Padding(
                  padding: EdgeInsets.only(top: 30),
                  child: Icon(Icons.sync, color: Colors.white),
                )
              )
            ),
          ]
        ),
        floatingActionButton: Padding(
          padding: EdgeInsets.only(bottom: 40),
          child: FloatingActionButton(
            backgroundColor: Colors.white,
            onPressed: () async {
              try {
                await _initializeControllerFuture;
                final path = join(
                  (await getTemporaryDirectory()).path, '${DateTime.now()}.png',
                );
                await _controller.takePicture(path);
                // final dir = await getTemporaryDirectory();
                // log("Pathh " + dir.path);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MessengerCameraDisplayPictureScreen(imagePath: path),
                  ),
                );
              }
              catch(e) {
                log(e.toString());
              }
            },
            child: Align(
              alignment: Alignment.center,
              child: Stack(
                children: [
                  Center(
                    child: Icon(MaterialCommunityIcons.circle, color: Colors.black, size: 55),
                  ),
                  Center(
                    child: Icon(MaterialCommunityIcons.circle, color: Colors.grey, size: 50)
                  )
                ]
              )
            )
          )
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      );
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _initCamera({cameraSelectedIdx}){
    _cameraSelectedIdx = cameraSelectedIdx;
    _controller = CameraController(
      widget.camera[cameraSelectedIdx],
      ResolutionPreset.max,
      enableAudio: false,
    );
    _initializeControllerFuture = _controller.initialize();
  }
  void _onSwitchCamera() {
    // log(_cameraSelectedIdx.toString() +"index");
    setState(
      () {
        _cameraSelectedIdx = (_cameraSelectedIdx < widget.camera.length-1) ? (_cameraSelectedIdx + 1) : 0;
        // log(_cameraSelectedIdx.toString() +"index");
        _controller = CameraController(
          widget.camera[_cameraSelectedIdx],
          ResolutionPreset.max,
          enableAudio: false,
        );
        _initializeControllerFuture = _controller.initialize();
      }
    );
  }
}