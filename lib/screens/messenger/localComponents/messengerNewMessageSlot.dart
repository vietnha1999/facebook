import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MessengerNewMessageSlot extends StatelessWidget {
  final Map<String,String> dataFriend;
  MessengerNewMessageSlot({@required this.dataFriend});
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: ClipOval(
        child: CachedNetworkImage(
          placeholder: (context, string) => Container(height: 50),
          imageUrl: dataFriend['imageUrl'],
          errorWidget: (context, url, error) => CircularProgressIndicator(),
          fit: BoxFit.cover,
          width: 50,
          height: 50,
        )
      ),
      title: Text(dataFriend['name'], style: TextStyle(fontWeight: FontWeight.bold)),
    );
  }
}