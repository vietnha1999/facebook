import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class MessengerMainBottomBar extends StatelessWidget {
  final int countNewMessage;
  MessengerMainBottomBar({@required this.countNewMessage});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Container()
          ),
          Expanded(
            flex: 1,
            child: Container(
              width: 50,
              height: 50,
              child: Align(
                alignment: Alignment.center,
                child: Stack(
                  overflow: Overflow.visible,
                  children: [
                    Icon(FontAwesome.comment, size: 35),
                    Positioned(
                      bottom: 14,
                      left: 22,
                      child: CircleAvatar(
                        radius: 10,
                        backgroundColor: Colors.red,
                        child: Center(
                          child: Text(countNewMessage.toString(), textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 10, fontWeight: FontWeight.bold))
                        ),
                      )
                    ),
                  ],
                )
              )
            )
          ),
          Expanded(
            flex: 1,
            child: Container()
          ),
          Expanded(
            flex: 1,
            child: Icon(MaterialIcons.people, size: 35, color: Colors.black38)
          ),
          Expanded(
            flex: 1,
            child: Container()
          ),
        ],
      )
    );
  }
}