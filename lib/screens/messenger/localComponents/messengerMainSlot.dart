import 'dart:developer';
import 'dart:ffi';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:facebook/actions/userInfoAction.dart';
import 'package:facebook/api/notification/notificationApi.dart';
import 'package:facebook/common/constants.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:intl/intl.dart';


class MessengerMainSlot extends StatelessWidget {
  final Map<String, String> dataFriend;
  final UserInfoState data;
  MessengerMainSlot({@required this.dataFriend, @required this.data});
  Widget _checkSeen(String USER_ID, FirebaseFirestore firestore, String boxId,  dynamic timeNew) {
    if (timeNew.get('fromUserId') == USER_ID) return Text(
      timeNew.get('content'),
      overflow: TextOverflow.ellipsis,
      maxLines: 1,
      style: TextStyle(fontSize: 14, fontWeight: (false) ? FontWeight.w900 : FontWeight.normal)
    );
    return StreamBuilder(
      stream: firestore.collection("boxes").doc(boxId).collection("info").doc("user"+USER_ID).snapshots(),
      builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (!snapshot.hasData || snapshot.data == null)
          return Center(
            child: CircularProgressIndicator()
          );
        Map<String, dynamic> newMessage = snapshot.data.data();
        if (newMessage == null || newMessage['createTime'] == null) {
          return Text(
            timeNew.get('content'),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: TextStyle(fontSize: 14, fontWeight: (true) ? FontWeight.w900 : FontWeight.normal)
          );
        }
        else {
          bool check = (timeNew.get('createTime').toDate().compareTo(newMessage['createTime'].toDate()) > 0);
          return Text(
            timeNew.get('content'),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: TextStyle(fontSize: 14, fontWeight: (check) ? FontWeight.w900 : FontWeight.normal)
          );
        }
      },
    );
    // var timeSeen = await firestore.collection("boxes").doc(boxId).collection("info").doc("newMessage").snapshots().
    // log(timeNew.get('createTime').toDate().toString()+"NEW");
    // return timeNew.get('fromUserId') == USER_ID || timeNew.get('createTime').toDate().compareTo(timeSeen['createTime$USER_ID'])  < 0;
  }
  Widget getLastMessage() {
    final String USER_ID = Constants.user_id.toString();
    final FirebaseFirestore firestore = FirebaseFirestore.instance;
    final String boxId = (int.parse(USER_ID) > int.parse(data.id))
        ? (USER_ID.toString() + "_" + data.id.toString())
        : (data.id.toString() + "_" + USER_ID.toString());
    log(boxId);
    return StreamBuilder(
      stream: firestore
          .collection("boxes")
          .doc(boxId)
          .collection("messages")
          .orderBy("createTime", descending: true)
          .snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData || snapshot.data == null)
          return Center(
            child: CircularProgressIndicator()
          );
        List<QueryDocumentSnapshot> messages = snapshot.data.docs;
        if (messages.length == 0) {
          return Center();
        }
        DateTime date = messages[0].get('createTime').toDate();
        return Row(
          children: [
            Expanded(
              child: _checkSeen(USER_ID, firestore, boxId, messages[0])
            ),
            Text('\u00B7 ' + DateFormat('dd-MM-yyyy hh:mm').format(date), textAlign: TextAlign.left, style: TextStyle(fontSize: 12))
          ],
        );
      }
    );
  }
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        log(data.id);
        chatFriendInfo = data;
        Navigator.pushNamed(context, MESSENGER_CHAT, arguments: {'friend': data});
      },
      leading: ClipOval(
        child: CachedNetworkImage(
          // imageUrl: dataFriend['imageUrl'],
          imageUrl: FileURI+data.avatar,
          errorWidget: (context, url, error) => CircularProgressIndicator(),
          fit: BoxFit.cover,
          width: 50,
          height: 50,
        )
      ),
      // title: Text(dataFriend['name'], style: TextStyle(fontWeight: FontWeight.bold)),
      title: Text(data.username, style: TextStyle(fontWeight: FontWeight.bold)),
      subtitle: getLastMessage(),
      trailing: ClipOval(
        child: CachedNetworkImage(
          // imageUrl: dataFriend['imageUrl'],
          imageUrl: FileURI+data.avatar,
          errorWidget: (context, url, error) => CircularProgressIndicator(),
          fit: BoxFit.cover,
          width: 16,
          height: 16,
        )
      ),
    );
  }
}