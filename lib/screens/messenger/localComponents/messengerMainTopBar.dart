import 'dart:developer';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:camera/camera.dart';
import 'package:facebook/api/notification/notificationApi.dart';
import 'package:facebook/common/constants.dart';
import 'package:facebook/middlewares/userInfoMiddleware.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:facebook/common/constants.dart' as Constants;


class MessengerMainTopBar extends StatelessWidget {
  final String imageAvatar;
  MessengerMainTopBar({@required this.imageAvatar});
  final USER_ID = Constants.user_id.toString();
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              InkWell(
                customBorder: CircleBorder(),
                onTap: () => Navigator.pushNamed(context, MESSENGER_SETTING, arguments: {"avatar": imageAvatar}),
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: ClipOval(
                    child: StoreConnector<AppState, UserInfoState>(
                      onInit: (store) async => await StoreProvider.of<AppState>(context).dispatch(getUserInfoThunk(USER_ID)),
                      distinct: true,
                      converter : (store) {
                        return store.state.userInfo;
                      },
                      builder : (context, viewModel) => CachedNetworkImage(
                        imageUrl: (viewModel.avatar == null) ? "https://cdn2.iconfinder.com/data/icons/font-awesome/1792/info-circle-512.png" : FileURI+viewModel.avatar,
                        errorWidget: (context, url, error) => CircularProgressIndicator(),
                        fit: BoxFit.cover,
                        width: 40,
                        height: 40,
                      )
                    )
                  ),
                ),
              ),
              Text("Chat", textAlign: TextAlign.left, style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold)),
            ]
          )
        ),
        Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              InkWell(
                customBorder: CircleBorder(),
                focusColor: Colors.blue,
                onTap: () async {
                  List<CameraDescription> cameras = await availableCameras();
                  Navigator.pushNamed(context, MESSENGER_CAMERA, arguments: {"camera": cameras});
                },
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(Icons.camera_alt, color: Colors.black)
                ),
              ),
              SizedBox(width: 10),
              InkWell(
                customBorder: CircleBorder(),
                onTap: () => log("Tap create message"),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(Icons.edit, color: Colors.black)
                )
              )
            ],
          )
        ),
      ],
    );
  }
}