import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class MessengerSettingUserInfo extends StatelessWidget {
  final String userAvatar;
  final String username;
  MessengerSettingUserInfo({@required this.userAvatar, @required this.username});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ClipOval(
            child: CachedNetworkImage(
              placeholder: (BuildContext context, String string) => Container(width: 100, height: 100),
              imageUrl: Constants.FileURI+userAvatar,
              errorWidget: (context, url, error) => Container(width: 100, height: 100, child: CircularProgressIndicator()),
              fit: BoxFit.cover,
              width: 100,
              height: 100,
            )
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 10),
          ),
          Text(username, textAlign: TextAlign.left, style: TextStyle(color: Colors.black, fontSize: 24, fontWeight: FontWeight.bold))
        ],
      )
    );
  }
}