import 'dart:developer';
import 'package:emoji_picker/emoji_picker.dart';
import 'package:facebook/common/constants.dart' as Constants;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:facebook/actions/userInfoAction.dart';
import 'package:facebook/api/notification/notificationApi.dart';
import 'package:flutter/material.dart';

class MessengerChatBottomBar extends StatefulWidget {
  const MessengerChatBottomBar({Key key}) : super(key: key);
  @override
  _MessengerChatBottombarState createState() => _MessengerChatBottombarState();
}


class _MessengerChatBottombarState extends State<MessengerChatBottomBar> {
  final _controller = new TextEditingController();
  bool _isEdit = false;
  bool isShowSticker = false;
  @override
  void initState() {
    super.initState();
    _isEdit = false;
    _controller.addListener(() {
      setState(
        () =>_isEdit = _controller.text.trim() != ""
      );
    });
  }
  @override
  void dispose() {
    _controller.dispose();
    _isEdit = false;
    isShowSticker = false;
    super.dispose();
  }
  void _onSendMessage() {
    if (_isEdit == true) {
      if (_controller.text != null && _controller.text.trim() != "") {
        final String USER_ID = Constants.user_id.toString();
        log("Tap to send message:\n" + _controller.text.trim());
        final FirebaseFirestore firestore = FirebaseFirestore.instance;
        final String boxId = ((int.parse(USER_ID) > int.parse(chatFriendInfo.id)))
            ? (USER_ID.toString() + "_" + chatFriendInfo.id.toString())
            : (chatFriendInfo.id.toString() + "_" + USER_ID.toString());

        firestore.collection("boxes").doc(boxId).collection("messages").add({
          "createTime": FieldValue.serverTimestamp(),
          "fromUserId": USER_ID,
          "content": _controller.text,
          "type": 0
        });
      }
    }
    else {
      final String USER_ID = Constants.user_id.toString();
      log("Tap to send message:\n" + _controller.text.trim());
      final FirebaseFirestore firestore = FirebaseFirestore.instance;
      final String boxId = ((int.parse(USER_ID) > int.parse(chatFriendInfo.id)))
          ? (USER_ID.toString() + "_" + chatFriendInfo.id.toString())
          : (chatFriendInfo.id.toString() + "_" + USER_ID.toString());

      firestore.collection("boxes").doc(boxId).collection("messages").add({
        "createTime": FieldValue.serverTimestamp(),
        "fromUserId": USER_ID,
        "content": "\u{1F44D}",
        "type": 1
      });
    }
    // log (_controller.toString());
    if (_controller.text != null && _controller.text.trim() != "") _controller.clear();
  }
  IconData _getIcon() {
    log("_getIcon");
    return (_isEdit == true) ? Icons.send : Icons.thumb_up;
  }
  Widget buildSticker() {
    return EmojiPicker(
      rows: 3,
      columns: 7,
      buttonMode: ButtonMode.MATERIAL,
      recommendKeywords: ["racing", "horse"],
      numRecommended: 10,
      onEmojiSelected: (emoji, category) {
        _controller.text = _controller.text + emoji.emoji;
        _controller.selection = TextSelection.fromPosition(TextPosition(offset: _controller.text.length));
      },
    );
  }
  Future<bool> onBackPress() {
    if (isShowSticker) {
      setState(() {
        isShowSticker = false;
      });
    } else {
      Navigator.pop(context);
    }

    return Future.value(false);
  }
  @override
  Widget build(BuildContext context) {
    final String USER_ID = Constants.user_id.toString();
    final FirebaseFirestore firestore = FirebaseFirestore.instance;
    final String boxId = ((int.parse(USER_ID) > int.parse(chatFriendInfo.id)))
          ? (USER_ID.toString() + "_" + chatFriendInfo.id.toString())
          : (chatFriendInfo.id.toString() + "_" + USER_ID.toString());
    return WillPopScope(
      onWillPop: onBackPress,
      child: Stack(
        children: <Widget>[
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin:  EdgeInsets.symmetric(horizontal: 1.0),
                    child:  IconButton(
                      icon:  Icon(Icons.face),
                      onPressed: () {
                        setState(() {
                          isShowSticker = !isShowSticker;
                        });
                      },
                      color: Colors.blueGrey,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(20)
                      ),
                      margin: EdgeInsets.only(bottom: 5),
                      child: Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: TextField (
                          cursorColor: Colors.blue,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Aa"
                          ),
                          autofocus: false,
                          onTap: () {
                            firestore.collection("boxes").doc(boxId).collection("info").doc('user'+USER_ID).set({'createTime': FieldValue.serverTimestamp()});
                          },
                          style: TextStyle(fontSize: 16),
                          // onSubmitted: (String text) => _onEditingChanged(),
                          controller: _controller,
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                        )
                      )
                    )
                  )),
                  Padding(
                    padding: EdgeInsets.all(0),
                    child: InkWell(
                      onTap: () => _onSendMessage(),
                      customBorder: CircleBorder(),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(4, 0, 8, 0),
                        child: Icon(_getIcon(), size: 30, color: Colors.blue)
                      )
                    )
                  )
                ]
              ),
              (isShowSticker ? buildSticker() : Container()),
            ]
          )
        ]
      )
    );
  }
}