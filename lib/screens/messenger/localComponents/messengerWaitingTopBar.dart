import 'package:flutter/material.dart';

class MessengerWaitingTopBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: Row(
                children: [
                  InkWell(
                    onTap: () => "Tap back",
                    customBorder: CircleBorder(),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 8, 14, 8),
                      child: Icon(Icons.arrow_back, size: 30)
                    )
                  ),
                  Text("Tin nhắn đang chờ", textAlign: TextAlign.left, style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold))
                ]
              )
            ),
            InkWell(
              onTap: () => "Tap more option",
              customBorder: CircleBorder(),
              child: Icon(Icons.more_vert, size: 30)
            )
          ],
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 0, 10),
            child: Text("BẠN CÓ THỂ BIẾT", style: TextStyle(color: Colors.black.withOpacity(0.6), fontSize: 15, fontWeight: FontWeight.w500, shadows: [Shadow(color: Colors.grey[100], offset: Offset(1,1))]))
          )
        )
      ]
    );
  }
}