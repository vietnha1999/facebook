import 'dart:developer';
import 'package:flutter/material.dart';

class MessengerTopBarOnlyTitle extends StatelessWidget {
  final String title;
  MessengerTopBarOnlyTitle({@required this.title});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 15, 8, 15),
      child: Row(
        children: [
          InkWell(
            onTap: () => Navigator.pop(context),
            customBorder: CircleBorder(),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 14, 8),
              child: Icon(Icons.arrow_back, size: 30)
            )
          ),
          Text(title, textAlign: TextAlign.left, style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold))
        ]
      )
    );
  }
}