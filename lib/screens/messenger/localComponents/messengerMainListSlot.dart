import 'dart:developer';

import 'package:facebook/api/notification/notificationApi.dart';
import 'package:facebook/middlewares/friendsMiddleware.dart';
import 'package:facebook/middlewares/userInfoMiddleware.dart';
import 'package:facebook/screens/messenger/localComponents/messengerMainSlot.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:facebook/common/constants.dart' as Constants;


class MessengerMainListSlot extends StatelessWidget {
  final List<Map<String, String>> data;
  MessengerMainListSlot({@required this.data});
  String USER_ID = Constants.user_id.toString();
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: StoreConnector<AppState, List<UserInfoState>>(
        onInit: (store) async {
          await StoreProvider.of<AppState>(context).dispatch(getUserInfoThunk(USER_ID));
          await StoreProvider.of<AppState>(context).dispatch(getFriendsThunk(USER_ID));
        },
        converter: (store) => store.state.friends,
        builder: (context, viewModel) => ListView.builder(
          padding: EdgeInsets.only(top: 0),
          itemCount: viewModel.length??0,
          itemBuilder: (BuildContext context, int index) {
            return MessengerMainSlot(dataFriend: data[index], data: viewModel[index]);
          }
        )
      )
    );
  }
}