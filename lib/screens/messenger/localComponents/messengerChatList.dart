import 'dart:developer';
import 'package:facebook/common/constants.dart' as Constants;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:facebook/actions/userInfoAction.dart';
import 'package:facebook/api/notification/notificationApi.dart';
import 'package:facebook/screens/chat/chat.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:flutter/material.dart';

class MessageChat {
  final int type;
  final String fromUserId;
  final String content;

  MessageChat(this.fromUserId, this.content, this.type);
}

class MessengerChatList extends StatelessWidget {
  final UserInfoState friend = chatFriendInfo;
  final String userId = Constants.user_id.toString();
  MessengerChatList();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    log("friendreal" + friend.id);
    return Expanded(
      child: buildListMessage()
    );
  }
  Widget buildItem(int index, message, context) {
    if (message.fromUserId == userId) {
      // Right (my message)
     if (message.type == 0) return Row(children: <Widget>[
        Container(
          constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width*5/6),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.blue
          ),
          margin: EdgeInsets.fromLTRB(0, 6, 0, 6),
          padding: EdgeInsets.all(10),
          child: Text(
            message.content,
            style: TextStyle(color: Colors.white, fontSize: 16),
            overflow: TextOverflow.visible,
          )
        ),
      ], mainAxisAlignment: MainAxisAlignment.end);
      else {
        return Row(children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.transparent
            ),
            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
            padding: EdgeInsets.all(3),
            child: Icon(Icons.thumb_up, size: 35, color: Colors.blue)
          ),
        ], mainAxisAlignment: MainAxisAlignment.end);
      }
    } else {
      // Left (peer message)
      if (message.type == 0) return Row(children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10),
          child: ClipOval(
            child: CachedNetworkImage(
              imageUrl: (friend == null) ? "https://i.pinimg.com/236x/f0/82/f9/f082f9421f5634fdc53bfa86b42b332f.jpg" : Constants.FileURI+friend.avatar,
              errorWidget: (context, url, error) => CircularProgressIndicator(),
              fit: BoxFit.cover,
              width: 30,
              height: 30,
            )
          )
        ),
        Row(children: <Widget>[
          Container(
            constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width*4/5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.grey[200]
            ),
            margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            padding: EdgeInsets.all(10),
            child: Text(
              message.content,
              style: TextStyle(color: Colors.black, fontSize: 16),
              overflow: TextOverflow.visible,
            )
          )
        ], crossAxisAlignment: CrossAxisAlignment.start)
      ]);
      else return Row(children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8),
          child: ClipOval(
            child: CachedNetworkImage(
              imageUrl: (friend == null) ? "https://i.pinimg.com/236x/f0/82/f9/f082f9421f5634fdc53bfa86b42b332f.jpg" : Constants.FileURI+friend.avatar,
              errorWidget: (context, url, error) => CircularProgressIndicator(),
              fit: BoxFit.cover,
              width: 30,
              height: 30,
            )
          )
        ),
        Row(children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.transparent
            ),
            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
            padding: EdgeInsets.all(3),
            child: Icon(Icons.thumb_up, size: 35, color: Colors.blue)
          ),
        ],crossAxisAlignment: CrossAxisAlignment.start)]);
    }
  }

  Widget buildListMessage() {
    final FirebaseFirestore firestore = FirebaseFirestore.instance;
    final String boxId = (int.parse(userId) > int.parse(friend.id))
        ? (userId.toString() + "_" + friend.id.toString())
        : (friend.id.toString() + "_" + userId.toString());
    return StreamBuilder(
        stream: firestore
            .collection("boxes")
            .doc(boxId)
            .collection("messages")
            .orderBy("createTime", descending: true)
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData || snapshot.data == null)
            return Center(
              child: CircularProgressIndicator()
            );
          List<QueryDocumentSnapshot> messages = snapshot.data.docs;
          if (messages.length == 0) {
            return Center();
          }
          return ListView.builder(
                  padding: EdgeInsets.all(10.0),
                  itemBuilder: (context, index) => buildItem(
                      index,
                      MessageChat(messages[index].get("fromUserId"),
                          messages[index].get("content"), messages[index].get("type")), context),
                  itemCount: messages.length,
                  reverse: true,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true);
        });
  }

}