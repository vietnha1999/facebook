import 'package:facebook/screens/messenger/localComponents/messengerMainBottomBar.dart';
import 'package:facebook/screens/messenger/localComponents/messengerMainListSlot.dart';
import 'package:facebook/screens/messenger/localComponents/messengerMainTopBar.dart';
import 'package:flutter/material.dart';

class MessengerMainScreen extends StatelessWidget {
  final List<Map<String, String>> dataFriend;
  final String userAva;
  final int countNewMessage;
  MessengerMainScreen({@required this.dataFriend, @required this.userAva, @required this.countNewMessage});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MessengerMainTopBar(imageAvatar: userAva),
                Divider(color: Colors.black38, thickness: 0.6, height: 0),
                MessengerMainListSlot(data: dataFriend),
                Divider(color: Colors.black38, thickness: 0.6, height: 0),
                MessengerMainBottomBar(countNewMessage: countNewMessage)
              ],
            )
          )
        ]
      )
    );
  }
}