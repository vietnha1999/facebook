import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Main4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Main4Demo(),
    );
  }
}

class Main4Demo extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<Main4Demo>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bài viết"),
    ),
    Tab(
      child: Text("Mọi người"),
    ),
    Tab(
      child: Text("Ảnh"),
    ),
    Tab(
      child: Text("Video"),
    ),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Create TabController for getting the index of current tab
    _controller = TabController(length: list.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 1, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              // actions: <Widget>[
              //   new IconButton(
              //     color: Colors.black,
              //     icon: new Icon(Icons.close),
              //     onPressed: () {},
              //   ),
              // ],
              title: new Container(
                child: new Text(
                  'Nhật ký hoạt động',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                padding: EdgeInsets.only(top: 0),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {}))),
          body: new TabBarView(
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.fromLTRB(0, 2, 0, 6),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: Color.fromRGBO(201, 204, 209, 1),
                              ),
                            ),
                          ),
                          child: Center(
                            child: Text(
                              'Xóa các tìm kiếm',
                              textScaleFactor: 1,
                              style: TextStyle(
                                color: Color.fromRGBO(90, 150, 209, 1),
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(14, 18, 10, 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                '7 Tháng 8 2020',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(14, 18, 10, 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                '6 Tháng 8 2020',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Column(
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                          child: Image.network(
                                            'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122186890_990477664786693_6592752700384066444_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=OgTuAYFqtBMAX8ciTTj&_nc_ht=scontent-hkt1-1.xx&oh=670faf1b8b2468d29d01483743fca544&oe=5FB9E2EC'
                                            ,width: 56,
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text('Bạn đã tìm kiếm trên Facebook'),
                                            Text(
                                              '"Sửa chữa nhà"',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      149, 149, 149, 1)),
                                            ),
                                            Container(
                                                child: Row(children: [
                                                  Image.network(
                                                    'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/cp0/122514315_690615788227185_95438967243289533_n.png?_nc_cat=102&ccb=2&_nc_sid=ae9488&_nc_ohc=_-ukU4uv8x0AX8BhfL_&_nc_ht=scontent-hkt1-1.xx&oh=2916df1a844e959ba20bf87b5b8127ef&oe=5FBAD3D9',
                                                    width: 15,
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      'Chỉ mình tôi * Đã ẩn khỏi dòng thời gian',
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Color.fromRGBO(
                                                              190, 190, 190, 1)),
                                                    )
                                                    ,
                                                    padding: EdgeInsets.only(top: 6),
                                                  )

                                                ]))
                                          ],
                                        ),
                                        Expanded(
                                          child: Text(""),
                                        ),
                                        new Align(
                                          alignment: Alignment.bottomRight,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            child: Icon(Icons.close),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(0),
                      color: Colors.white,
                    ),
                  ),
                  // Center(
                  //   child: Text('Tất cả'),
                  // ),
                  color: Color.fromRGBO(201, 204, 209, 1),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
