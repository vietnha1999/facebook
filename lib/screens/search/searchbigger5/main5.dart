import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class MyApp5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TabBarDemo(),
    );
  }
}

class TabBarDemo extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<TabBarDemo>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bài viết"),
    ),
    Tab(
      child: Text("Mọi người"),
    ),
    Tab(
      child: Text("Ảnh"),
    ),
    Tab(
      child: Text("Video"),
    ),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Create TabController for getting the index of current tab
    _controller = TabController(length: list.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 1, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              // actions: <Widget>[
              //   new IconButton(
              //     color: Colors.black,
              //     icon: new Icon(Icons.close),
              //     onPressed: () {},
              //   ),
              // ],
              title: new Container(
                child: new Text(
                  'Nhật ký hoạt động',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                padding: EdgeInsets.only(top: 0),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {}))),
          body: new TabBarView(
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.fromLTRB(0, 2, 0, 2000),
                  child: Container(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Center(
                        child: IconButton(
                          iconSize: 12,
                          color: Color.fromRGBO(206, 206, 206, 1),
                          icon: new Icon(Icons.circle),
                          onPressed: () {},
                        ),
                      ),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(0),
                      color: Colors.white,
                    ),
                  ),
                  // Center(
                  //   child: Text('Tất cả'),
                  // ),
                  color: Color.fromRGBO(201, 204, 209, 1),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
