import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/screens/search/listpost/ListPostSearch.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class SearchFuture extends StatefulWidget {
  Future<SearchResponse> searchResponse;
  SearchFuture({@required this.searchResponse});
  _TabBarDemoState s;

  reload() {
    s.setState(() {});
  }

  @override
  _TabBarDemoState createState() {
    this.s = new _TabBarDemoState(this.searchResponse);
    return s;
  }
}

class _TabBarDemoState extends State<SearchFuture> {
  Future<SearchResponse> searchResponse;
  SearchResponse searchResponseS;
  _TabBarDemoState(@required this.searchResponse);

  @override
  Widget build(BuildContext context) {
    if (searchResponseS != null)
      Constants.lastIdPost = searchResponseS.data.last.id;
    return Container(
        child:
            // searchResponseS!=null?
            //
            FutureBuilder<SearchResponse>(
      future: searchResponse,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          Constants.lastIdPost = snapshot.data.data.last.id;
          print("___________________________________________________");
          print(snapshot.data.data.last.id);
          if (searchResponseS == null) {
            searchResponseS = snapshot.data;
          }
          return ListPostSearch(searchResponseS.data);
        } else if (snapshot.hasError) {
          return Text("Loi");
        }
        // By default, show a loading spinner.
        if (searchResponseS != null) {
          return ListPostSearch(searchResponseS.data);
        }

        return Center(
          child: CircularProgressIndicator(),
        );
      },
    ));
  }
}
