import 'package:flutter/material.dart';
class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final _formKey = new GlobalKey<FormState>();
  String oldPassword;
  String newPassword;
  String newPasswordAgain; // nhập lại password

  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController newPasswordAgainController = TextEditingController();

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
  void validateAndSubmit() {
    if (validateAndSave()) {
      setState(() {
        oldPassword = oldPasswordController.text;
        newPasswordAgain = newPasswordAgainController.text;
        newPassword = newPasswordController.text;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.white,
        title: Text("Đổi mật khẩu", style: TextStyle(fontSize: 22, fontWeight: FontWeight.w400, color: Colors.black),),
        leading: IconButton(
          padding: EdgeInsets.only(left: 5),
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    _showFormPassword(),
                    Container(height: 10,),
                    _showFormNewPassword(),
                    Container(height: 10,),
                    _showFormNewPasswordAgain(),
                    Container(height: 10,),
                    Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: 20, top: 10, bottom: 10, right: 20),
                            height: 64,
                            width: size.width ,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0),
                                  side: BorderSide(color: Color.fromRGBO(23, 119, 242, 1))),
                              color: Color.fromRGBO(23, 119, 242, 1),
                              onPressed: () {
                                // addFriendButton();
                              },
                              child: Container(
                                padding: EdgeInsets.only(left: size.width*0.2),
                                child: Row(
                                  children: <Widget>[

                                    Container(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        "Cập nhật mật khẩu",
                                        style: TextStyle(fontSize: 18, color: Colors.white),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ]
                    ),
                    Container(height: 2,),
                    Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: 20, top: 10, bottom: 10, right: 20),
                            height: 64,
                            width: size.width ,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.0),
                                  side: BorderSide(color: Color.fromRGBO(230, 235, 242, 1))),
                              color: Color.fromRGBO(230, 235, 242, 1),
                              onPressed: () {
                                // addFriendButton();
                              },
                              child: Container(
                                padding: EdgeInsets.only(left: size.width*0.35),
                                child: Row(
                                  children: <Widget>[

                                    Container(

                                      child: Text(
                                        "Hủy",
                                        style: TextStyle(fontSize: 18, color: Colors.black),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ]
                    ),
                    Container(height: 2,),
                    Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: size.width*0.32),
                            child: InkWell(
                              onTap: (){},
                              child: Text("Quên mật khẩu ?", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Color.fromRGBO(23, 119, 242, 1)),),
                            )
                        )
                      ],
                    )
                  ],
                )
            ),


          ],
        ),
      )

    );
  }
  Widget _showFormPassword(){
    double width = MediaQuery
        .of(context)
        .size
        .width;
    double height = MediaQuery
        .of(context)
        .size
        .height;
    return new Container(
      padding: EdgeInsets.only(left: 20, right: 20, top: 10),
      width: width,
      child: TextFormField(

        controller: oldPasswordController,
        decoration: InputDecoration(
          prefixIcon:Icon(Icons.lock_open, size: 27),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              borderSide: const BorderSide(
                color: Colors.grey,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              borderSide: BorderSide(color: Color.fromRGBO(24, 119, 242, 1)),
            ),
            labelText: 'Mật khẩu hiện tại',
            labelStyle: TextStyle(
              fontSize: 20,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(153, 156, 160, 1)),
            ),
        validator: (value) => value.isEmpty ? 'Vui lòng nhập mật khẩu của bạn' : null,
        onSaved: (value) => oldPassword = value.trim(),
      ),
    );
  }
  Widget _showFormNewPassword(){
    double width = MediaQuery
        .of(context)
        .size
        .width;
    double height = MediaQuery
        .of(context)
        .size
        .height;
    return new Container(
      padding: EdgeInsets.only(left: 20, right: 20, top: 10),
      width: width ,
      child: TextFormField(
        controller: newPasswordController,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.vpn_key, size:27),
            labelText: 'Mật khẩu mới',
            labelStyle: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(153, 156, 160, 1)),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            borderSide: const BorderSide(
              color: Colors.grey,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            borderSide: BorderSide(color: Color.fromRGBO(24, 119, 242, 1)),
          ),
        ),
        validator: (value) => value.isEmpty ? 'Vui lòng nhập mật khẩu mới của bạn' : null,
        onSaved: (value) => newPassword = value.trim(),
      ),
    );
  }
  Widget _showFormNewPasswordAgain(){
    double width = MediaQuery
        .of(context)
        .size
        .width;
    double height = MediaQuery
        .of(context)
        .size
        .height;
    return new Container(
      padding: EdgeInsets.only(left: 20, right: 20, top: 10),
      width: width ,
      child: TextFormField(
        controller: newPasswordAgainController,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.vpn_key, size:27),
            labelText: 'Nhập lại mật khẩu mới',
            labelStyle: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(153, 156, 160, 1)),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            borderSide: const BorderSide(
              color: Colors.grey,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            borderSide: BorderSide(color: Color.fromRGBO(24, 119, 242, 1)),
          ),),
        validator: (value) => value.isEmpty ? 'Mật khẩu phải trùng với mật khẩu ở trên' : null,
        onSaved: (value) => newPasswordAgain = value.trim(),
      ),
    );
  }

}
