import 'package:facebook/api/friend/set_accept_friend.dart';
import 'package:facebook/api/payload/FriendsData.dart';
import 'package:facebook/router/Router.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class SingleLoiMoi extends StatefulWidget {
  final FriendsData data;
  int accept;
  SingleLoiMoi(@required this.data, this.accept);
  @override
  _MyListState createState() => _MyListState(data, accept);
}

class _MyListState extends State<SingleLoiMoi> {
  final FriendsData data;
  int accept;
  _MyListState(@required this.data, this.accept);

  
  void acceptFriend(int id) {
    setState(() => this.accept = 1);
    print("_____________________");
    set_accept_friend(Constants.authorization, Constants.token, id, 1);
  }

  void rejectFriend(int id) {
    setState(() => this.accept = 2);
    print("_____________________");
    set_accept_friend(Constants.authorization, Constants.token, id, 0);
  }

  toPersonalPage() {
    Navigator.pushNamed(context, PERSONAL_PAGE,
                                          arguments: {
                                            "id": data.id
                                          });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: (){
              toPersonalPage();
            },
            child: CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage(Constants.FileURI + data.avatar)),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IntrinsicHeight(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: (){
                          toPersonalPage();
                        },
                        child: Container(
                        child: Text(
                          data.username,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      ),
                      ),                                                                            
                      Container(
                        child: Text(
                          "2 năm",
                          style: TextStyle(),
                        ),
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      ),
                    ],
                  ),
                ),
                accept == 0
                    ? Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 0),
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  color: Color.fromRGBO(24, 120, 243, 1),
                                  textColor: Colors.red,
                                  padding: EdgeInsets.fromLTRB(22, 0, 22, 0),
                                  onPressed: ()=>acceptFriend(data.id),
                                  child: Text(
                                    "Chấp nhận",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  color: Color.fromRGBO(229, 230, 235, 1),
                                  textColor: Colors.red,
                                  padding: EdgeInsets.fromLTRB(22, 0, 22, 0),
                                  onPressed: () => rejectFriend(data.id),
                                  child: Text(
                                    "Xóa",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                      )
                    : Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: accept == 1
                              ? Text(
                                  "Các bạn đã là bạn bè",
                                  style: TextStyle(),
                                )
                              : Text(
                                  "Đã gỡ lời mời",
                                  style: TextStyle(),
                                ),
                        )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}