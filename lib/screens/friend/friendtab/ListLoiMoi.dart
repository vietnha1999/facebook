import 'package:facebook/api/payload/FriendsData.dart';
import 'package:facebook/screens/friend/friendtab/SingleLoiMoi.dart';
import 'package:flutter/material.dart';

class ListLoiMoi extends StatefulWidget {
  final List<FriendsData> data;
  ListLoiMoi(@required this.data);
  @override
  _MyListState createState() => _MyListState(data);
}

class _MyListState extends State<ListLoiMoi> {
  final List<FriendsData> data;
  List<int> accepts = new List<int>();
  _MyListState(@required this.data);
  @override
  Widget build(BuildContext context) {
    for (int i = 0; i < data.length; i++) {
      accepts.add(0);
    }
    return Container(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
        color: Colors.white,
        child: Column(children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                child: Text(
                  "Lời mời kết bạn",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                  child: Container(
                padding: EdgeInsets.only(top: 4),
                child: Text(
                  " " + data.length.toString(),
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(220, 47, 74, 1),
                  ),
                ),
              )),
              Align(
                alignment: Alignment.topRight,
                child: Container(
                    padding: EdgeInsets.fromLTRB(0, 5, 10, 0),
                    child: Text(
                      "Xem tất cả",
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
              ),
            ],
          ),
          ListView.builder(
            itemCount: data.length,
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 0),
            itemBuilder: (BuildContext context, int index) {
              return SingleLoiMoi(data[index], accepts[index]);
            },
          )
        ]));
  }
}
