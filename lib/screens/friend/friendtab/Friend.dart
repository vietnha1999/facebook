import 'package:facebook/api/friend/get_requested_friends.dart';
import 'package:facebook/api/payload/GetRequestFriendResponse.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/friend/friendtab/FriendContainter.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class Friend extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold (
      body: TabBarDemo(),
    );
  }
}

class TabBarDemo extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<TabBarDemo>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;
  Future<GetRequestFriendResponse> getFriendResponse;
  int user_id;
  String token;
  String authorization;

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bài viết"),
    ),
    Tab(
      child: Text("Mọi người"),
    ),
    Tab(
      child: Text("Ảnh"),
    ),
    Tab(
      child: Text("Video"),
    ),
  ];

  @override
  void initState() {
    super.initState();
    authorization = Constants.authorization;
    token = Constants.token;
    user_id = 6;
    getFriendResponse = get_requested_friends(authorization, token, user_id)
        as Future<GetRequestFriendResponse>;
    _controller = TabController(length: list.length, vsync: this);
    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      body: DefaultTabController(
        length: 5,
        child: Scaffold(
          appBar: new PreferredSize(
            child: Container(
              padding: EdgeInsets.only(top: 20),
              child: TabBar(
                isScrollable: true,
                unselectedLabelColor: Colors.grey,
                labelColor: Colors.blue,
                tabs: [
                  Container(
                    width: 40,
                    height: 50,
                    alignment: Alignment.center,
                    child: Tab(
                      child: Icon(
                        Icons.home,
                        // color: Color.fromRGBO(102, 102, 107, 1),
                      ),
                    ),
                  ),
                  Container(
                    width: 40,
                    height: 50,
                    alignment: Alignment.center,
                    child: Tab(
                      child: Image.network(
                        'https://scontent.fhan4-1.fna.fbcdn.net/v/t1.15752-9/cp0/122351598_452928535676522_8773025976053496690_n.png?_nc_cat=105&ccb=2&_nc_sid=ae9488&_nc_ohc=kRQ0Qj1chZQAX-SUIUk&_nc_ht=scontent.fhan4-1.fna&oh=b19cf3af0d4a252ed5a036d6bdf0b61d&oe=5FE56F18',
                        width: 25,
                      ),
                    ),
                  ),
                  Container(
                    width: 40,
                    height: 50,
                    alignment: Alignment.center,
                    child: Tab(
                      child: Image.network(
                        'https://scontent.fhan2-6.fna.fbcdn.net/v/t1.15752-9/cp0/122344181_359954085217201_2169194832696780049_n.png?_nc_cat=100&ccb=2&_nc_sid=ae9488&_nc_ohc=gBzyKj__3TgAX952Cz2&_nc_ht=scontent.fhan2-6.fna&oh=c2ff3901096e20b80b5de9aa9e68d558&oe=5FBBB3A5',
                        width: 25,
                      ),
                    ),
                  ),
                  Container(
                    width: 40,
                    height: 50,
                    alignment: Alignment.center,
                    child: Tab(
                      child: Image.network(
                        'https://scontent.fhan2-1.fna.fbcdn.net/v/t1.15752-9/cp0/122431656_991855511339626_2967317248140889958_n.png?_nc_cat=106&ccb=2&_nc_sid=ae9488&_nc_ohc=QEkHnGxl2mEAX-nTQnG&_nc_ht=scontent.fhan2-1.fna&oh=6defd00b2ef01ff779e59670eb79db7d&oe=5FBC6D2A',
                        width: 25,
                      ),
                    ),
                  ),
                  Container(
                    width: 40,
                    height: 50,
                    alignment: Alignment.center,
                    child: Tab(
                      child: Icon(
                        Icons.notifications_none,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            preferredSize: Size.fromHeight(40.0),
          ),
          backgroundColor: Colors.white,
          body: new TabBarView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(8, 6, 8, 6),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          'Tất cả',
                          textScaleFactor: 1.5,
                          style: TextStyle(color: Colors.black),
                        ),
                        padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                      ),
                    ],
                  ),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                    color: Color.fromRGBO(201, 204, 209, 1),
                    child: 
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                      color: Colors.white,
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                                child: Text(
                                  "Bạn bè",
                                  style: TextStyle(
                                      fontSize: 32,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Expanded(
                                child: Text(""),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 0.0),
                                    child: RawMaterialButton(
                                      onPressed: () {Navigator.pushNamed(context, SEARCH_POST);},
                                      elevation: 2.0,
                                      fillColor:
                                          Color.fromRGBO(229, 230, 235, 1),
                                      child: Icon(
                                        Icons.search,
                                        // size: 5.0,
                                      ),
                                      padding: EdgeInsets.all(4.0),
                                      shape: CircleBorder(),
                                    )),
                              ),
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  color: Color.fromRGBO(229, 230, 235, 1),
                                  textColor: Colors.red,
                                  padding: EdgeInsets.all(14.0),
                                  onPressed: () {},
                                  child: Text(
                                    "Gợi ý",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14.0,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  color: Color.fromRGBO(229, 230, 235, 1),
                                  textColor: Colors.red,
                                  padding: EdgeInsets.all(14.0),
                                  onPressed: () {
                                    Navigator.pushNamed(context, ALL_FRIEND);
                                  },
                                  child: Text(
                                    "Tất cả bạn bè",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14.0,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  
                  ),
                  FriendContainer(getFriendResponse: getFriendResponse),
                ],
              ),
              
              Container(
                child: Center(
                  child: Text('Mọi người'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              Container(
                child: Center(
                  child: Text('Ảnh'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              Container(
                child: Center(
                  child: Text('Video'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
            
            ],
          
          ),
        ),
      ),
    );
  }
}
