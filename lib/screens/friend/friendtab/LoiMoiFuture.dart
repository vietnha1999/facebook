import 'package:facebook/api/payload/GetRequestFriendResponse.dart';
import 'package:facebook/screens/friend/friendtab/ListLoiMoi.dart';
import 'package:flutter/material.dart';

class LoiMoiFuture extends StatelessWidget {
  final Future<GetRequestFriendResponse> loiMoiResponse;
  LoiMoiFuture({@required this.loiMoiResponse});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<GetRequestFriendResponse>(
        future: loiMoiResponse,
        builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListLoiMoi(snapshot.data.data.request);
            } else if (snapshot.hasError) {
              return Text("Loi");
            } 
            // else if(!snapshot.hasData) {
            //   return Container(
            //   padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
            //   color: Colors.white,
            //   child: Text("Không có lời mời kết bạn nào")
            //   );
            // }
          // By default, show a loading spinner.
          return Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
              color: Colors.white,
              child: CircularProgressIndicator()
          );
        },
      ),
    );
  }
}
