import 'package:facebook/api/payload/FriendsData.dart';
import 'package:facebook/screens/friend/allfriend/SingleAllFriend.dart';
import 'package:facebook/screens/friend/friendtab/SingleLoiMoi.dart';
import 'package:flutter/material.dart';

class ListAllFriend extends StatefulWidget {
  final List<FriendsData> data;
  ListAllFriend(@required this.data);
  @override
  _MyListState createState() => _MyListState(data);
}

class _MyListState extends State<ListAllFriend> {
  final List<FriendsData> data;
  _MyListState(@required this.data);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
            itemCount: data.length,
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 0),
            itemBuilder: (BuildContext context, int index) {
              return SingleAllFriend(data[index]);
            },
          );
  }
}
