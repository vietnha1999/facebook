
import 'package:facebook/api/friend/get_user_friends.dart';
import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/friend/allfriend/AllFriendContainer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class AllFriend extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<AllFriend>
    with SingleTickerProviderStateMixin {
  Future<GetFriendResponse> getFriendResponse;
  int user_id;
  String token;
  String authorization;
  int index;
  int count;
  @override
  void initState() {
    // TODO: implement initState
    authorization = Constants.authorization; 
    token = Constants.token; 
    user_id = Constants.user_id;
    getFriendResponse = get_user_friends(
        authorization,
        token,
        user_id, index, count) as Future<GetFriendResponse>;
    super.initState();
    // Create TabController for getting the index of current tab
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      body: DefaultTabController(
        length: 1, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              actions: [
                IconButton(
                    color: Colors.black,
                    icon: Icon(Icons.search),
                    onPressed: () {
                      Navigator.pushNamed(context, SEARCH_POST);
                    }),
              ],
              elevation: 0,
              backgroundColor: Colors.white,
              title: new Container(
                child: new Text(
                  'Tất cả bạn bè',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                padding: EdgeInsets.only(top: 0),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {Navigator.pop(context);}))),
          backgroundColor: Colors.white,
          body: new TabBarView(
            children: <Widget>[
                AllFriendContainer(getFriendsResponse: getFriendResponse,),
            ],
          ),
        ),
      ),
    );
  }
}
