import 'package:facebook/api/friend/set_accept_friend.dart';
import 'package:facebook/api/friend/set_request_friend.dart';
import 'package:facebook/api/payload/FriendsData.dart';
import 'package:facebook/router/Router.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class SuggestSingle extends StatefulWidget {
  final FriendsData data;
  SuggestSingle(@required this.data);
  @override
  _MyListState createState() => _MyListState(data);
}

class _MyListState extends State<SuggestSingle> {
  final FriendsData data;
  int accept = 0;
  _MyListState(@required this.data);

  
  void acceptFriend(int id) {
    setState(() => this.accept = 1);
    print("_____________________");
    set_request_friend(Constants.authorization, Constants.token, id);
  }

  void rejectFriend(int id) {
    setState(() => this.accept = 2);
  }

  toPersonalPage() {
    Navigator.pushNamed(context, PERSONAL_PAGE,
                                          arguments: {
                                            "id": data.id
                                          });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: (){
              toPersonalPage();
            },
            child: CircleAvatar(
              radius: 50,
              backgroundImage: 
              data.avatar!=null?
              NetworkImage(Constants.FileURI + data.avatar):
              NetworkImage('https://1.bp.blogspot.com/-m3UYn4_PEms/Xnch6mOTHJI/AAAAAAAAZkE/GuepXW9p7MA6l81zSCnmNaFFhfQASQhowCLcBGAsYHQ/s1600/Cach-Lam-Avatar-Dang-Hot%2B%25281%2529.jpg')
              ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IntrinsicHeight(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: (){
                          toPersonalPage();
                        },
                        child: Container(
                        child: Text(
                          data.username,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      ),
                      ),                                                                            
                      
                    ],
                  ),
                ),
                accept == 0
                    ? Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 0),
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  color: Color.fromRGBO(24, 120, 243, 1),
                                  textColor: Colors.red,
                                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  onPressed: ()=>acceptFriend(data.id),
                                  child: Text(
                                    "Thêm bạn bè",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  color: Color.fromRGBO(229, 230, 235, 1),
                                  textColor: Colors.red,
                                  padding: EdgeInsets.fromLTRB(22, 0, 22, 0),
                                  onPressed: () => rejectFriend(data.id),
                                  child: Text(
                                    "Gỡ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                      )
                    : Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: accept == 1
                              ? Text(
                                  "Đã gửi yêu cầu",
                                  style: TextStyle(),
                                )
                              : Text(
                                  "Đã gỡ lời mời",
                                  style: TextStyle(),
                                ),
                        )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}