import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class SingleEmoji extends StatelessWidget {
  final String data;
  String image;

  SingleEmoji(this.data);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pop(context, data);
      },
      child: Row(
        children: [
          Image.network(
            Constants.fakeImage,
            width: 30,
          ),
          Text(data)
        ],
      ),
    );
  }
}
