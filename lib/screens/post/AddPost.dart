import 'dart:io';
import 'dart:math';

import 'package:chewie/chewie.dart';
import 'package:facebook/api/friend/get_user_friends.dart';
import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:facebook/router/Router.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:http_parser/http_parser.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:file_picker/file_picker.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as p;
import 'package:video_player/video_player.dart';

class AddPost extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<AddPost>
    with SingleTickerProviderStateMixin {
  Future<GetFriendResponse> getFriendResponse;
  int user_id;
  String token;
  String authorization;
  int index;
  int count;
  final content = TextEditingController();
  String statusOrActivity;
  VideoPlayerController _video_controller;
  List<File> media = new List<File>();
  List<String> mediaPath = new List<String>();

  showAlertDialog(BuildContext context, String text) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      // title: Text("My title"),
      content: Text(text),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  void dispose() {
    _video_controller.dispose();
    super.dispose();
  }

  void getFile(BuildContext context) async {
    try {
      File file = await FilePicker.getFile(type: FileType.custom);
      _video_controller = VideoPlayerController.file(file)
        ..initialize().then((_) {
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          setState(() {});
        });
      String path = p.extension(file.path);
      print("@@@@@@@@@@@@@@@@@@@@@@@@");
      print(path);
      if (path != ".mp4" &&
          path != ".jpg" &&
          path != ".png" &&
          path != ".jpeg") {
        showAlertDialog(context, "Chỉ được chọn ảnh hoặc video");
        return;
      }
      if (this.media.length > 0) {
        if (p.extension(media[0].path) == ".mp4") {
          if (p.extension(file.path) != p.extension(media[0].path)) {
            showAlertDialog(context, "Không cho phép chọn video cùng với ảnh");
            return;
          }
          if (p.extension(file.path) == p.extension(media[0].path)) {
            showAlertDialog(context, "Chỉ được chọn tối đa 1 video");
            return;
          }
        }
      }
      if (file == null) {
        return;
      }
      if (this.media.length == 4) {
        showAlertDialog(context, "Chỉ được chọn tối đa 4 ảnh");
        return;
      }
      setState(() {
        this.media.add(file);
      });
    } on PlatformException catch (e) {
      print("Error while picking the file: " + e.toString());
    }
  }

  void getFilePath() async {
    try {
      String file = await FilePicker.getFilePath(type: FileType.custom);
      if (file == null) {
        return;
      }
      setState(() {
        this.mediaPath.add(file);
      });
    } on PlatformException catch (e) {
      print("Error while picking the file: " + e.toString());
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    authorization = Constants.authorization;
    token = Constants.token;
    user_id = Constants.user_id;
    // getFriendResponse =
    //     get_user_friends(authorization, token, user_id, index, count)
    //         as Future<GetFriendResponse>;
    super.initState();
    // Create TabController for getting the index of current tab
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Scaffold(
      body: DefaultTabController(
        length: 1, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              actions: [
                InkWell(
                  onTap: () async {
                    Uri uri = Uri.parse(Constants.Root + "api/add_post");
                    http.MultipartRequest request =
                        new http.MultipartRequest('POST', uri);
                    request.headers['Authorization'] = Constants.token;
                    request.fields['token'] = Constants.token;
                    request.fields['described'] = content.text;
                    if (statusOrActivity != null) {
                      request.fields['status'] = statusOrActivity;
                    }
                    for (var m in media) {
                      request.files.add(
                          await http.MultipartFile.fromPath('files', m.path));
                    }
                    http.StreamedResponse response = await request.send();
                    var responsebody = await http.Response.fromStream(response);
                    print(responsebody.body);
                    Navigator.pop(context);
                  },
                  child: Container(
                    padding: EdgeInsets.only(top:10, right: 10),
                    child: Text(
                      'Đăng',
                      style: TextStyle(color: (content.text != null) ? Colors.blue: Colors.black,fontSize: 20),
                    ),
                  ) 
                  
                )
              ],
              elevation: 2,
              backgroundColor: Colors.white,
              title: new Container(
                child: new Text(
                  'Tạo bài viết',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                padding: EdgeInsets.only(top: 0),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigator.pop(context);
                      }))),
          backgroundColor: Colors.white,
          body: SlidingUpPanel(
              panel: Container(
                  child: Column(children: [
                Icon(Icons.arrow_drop_up_outlined),
                InkWell(
                  onTap: () {
                    getFile(context);
                    // getFilePath();
                  },
                  child: Row(
                    children: [
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Image.asset("assets/Icon/iconImage.PNG"),
                            width: 40,
                            height: 40,
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Text("Ảnh/Video",style: TextStyle(fontSize: 19),),
                          )
                        ],
                      ),


                    ],
                  ),
                ),

                        Divider(
                          indent: 8,
                          endIndent: 8,
                          thickness: 0.5,
                          color: Colors.grey,
                        )
                    ,
                Row(children: [
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Image.asset("assets/Icon/iconBanbe.PNG"),
                        width: 40,
                        height: 40,
                      ),
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Text("Gắn thẻ bạn bè",style: TextStyle(fontSize: 19),),
                      )
                    ],
                  ),
                ]),
                    Divider(
                      indent: 8,
                      endIndent: 8,
                      thickness: 0.5,
                      color: Colors.grey,
                    )
                    ,
                InkWell(
                    onTap: () async {
                      var statusOrActivity = await Navigator.pushNamed(
                          context, EMOJI_AND_ACTIVITY);
                      setState(() {
                        this.statusOrActivity = statusOrActivity;
                      });
                      print(this.statusOrActivity);
                    },
                    child: Row(children: [
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Image.asset("assets/Icon/iconMatcuoi.PNG"),
                            width: 40,
                            height: 40,
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Text("Cảm xúc hoạt động",style: TextStyle(fontSize: 19),),
                          )
                        ],
                      ),
                    ])),
                    Divider(
                      indent: 8,
                      endIndent: 8,
                      thickness: 0.5,
                      color: Colors.grey,
                    )
                    ,
                    Row(children: [
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Image.asset("assets/Icon/15.PNG"),
                            width: 40,
                            height: 40,
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Text("Checkin",style: TextStyle(fontSize: 19),),
                          )
                        ],
                      ),
                    ]),
                    Divider(
                      indent: 8,
                      endIndent: 8,
                      thickness: 0.5,
                      color: Colors.grey,
                    )
                    ,
                    Row(children: [
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Image.asset("assets/Icon/16.PNG"),
                            width: 40,
                            height: 40,
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Text("Camera",style: TextStyle(fontSize: 19),),
                          )
                        ],
                      ),
                    ]),
              ])),
              body: SingleChildScrollView(
                child: Container(
                  // height: 1400,
                  child: Column(children: [

                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 7,left: 7,right: 7,bottom: 7),
                          child: CircleAvatar(
                            radius: 35,
                            backgroundImage: 
                            Constants.avatar==null?
                            AssetImage("assets/ramdom.jpg"):
                            NetworkImage(Constants.FileURI + Constants.avatar)
                            ,
                          ),
                        ),

                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(right:size.width*0.25 ),
                                  child: Text(Constants.userName,style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,color: Colors.black),),

                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(right: 2),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8.0),
                                        border: Border.all(
                                          color: Colors.grey, //                   <--- border color
                                          width: 1.0,
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            padding:EdgeInsets.only(left: 3,top: 5,bottom: 3,right: 3),
                                            child: Icon(Icons.people,size: 17,color: Colors.grey,),
                                          ),
                                          Container(
                                              padding: EdgeInsets.only(right: 3),
                                              child:Text(
                                                "Bạn bè", style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey, fontSize: 15),
                                              )
                                          ),
                                          Container(
                                            child: Icon(Icons.arrow_drop_down,size: 17, color: Colors.grey,),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                      width: 5,
                                    )
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8.0),
                                        border: Border.all(
                                          color: Colors.grey, //                   <--- border color
                                          width: 1.0,
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            padding:EdgeInsets.only(left: 3,top: 5,bottom: 3,right: 3),
                                            child: Icon(Icons.add,size: 17,color: Colors.grey,),
                                          ),
                                          Container(
                                              padding: EdgeInsets.only(right: 3),
                                              child:Text(
                                                "Album", style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey, fontSize: 15),
                                              )
                                          ),
                                          Container(

                                            child: Icon(Icons.arrow_drop_down,size: 17, color: Colors.grey,),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),


                              ],
                            )
                          ],
                        ),
                        statusOrActivity != null
                            ? Text("đang cảm thấy " + statusOrActivity)
                            : Text(''),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20,top: 10, right:20),
                      height: 100.0,
                      child: TextField(
                        maxLength: 600,
                        controller: content,
                        decoration: InputDecoration(
                          hintText: 'Bạn đang nghĩ gì',
                          hintStyle: TextStyle(fontSize: 24,),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                        ),
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                    // Center(
                    //   child: _file == null
                    //       ? new Text('No file selected.')
                    //       : new Image.file(_file),
                    // ),
                    Container(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 200),
                        child: media.length == 0
                            ? new Text('No file selected.')
                            : Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  if (media.length >= 1 &&
                                      p.extension(media[0].path) == ".mp4")
                                    // Scaffold(
                                    //   body:
                                    Column(
                                      children: [
                                        Container(
                                          height: MediaQuery.of(context).size.height*1,
                                          width: MediaQuery.of(context).size.width*1,
                                          child: Center(
                                            child: FittedBox(
                                              fit: BoxFit.fill,
                                              child: Chewie(
                                                controller: ChewieController(
                                                  videoPlayerController: _video_controller,
                                                  aspectRatio: _video_controller.value.aspectRatio,
                                                  autoPlay: false,
                                                  looping: false,
                                                ),
                                              ),
                                            ),
                                            ),
                                          ),
                                      ],
                                    ),

                                  // ),
                                  if (media.length >= 1 &&
                                      p.extension(media[0].path) != ".mp4")
                                    Image.file(
                                      media[0],
                                      // height: (105 * max(1, 3 / media.length))
                                      //     .toDouble(),
                                      width: (105 * max(1, 3 / media.length))
                                          .toDouble(),
                                    ),
                                  if (media.length >= 2)
                                    Image.file(media[1],
                                        // height: (105 * max(1, 1.5*2/media.length)).toDouble(),
                                        width: (105 *
                                                max(1, 1.5 * 2 / media.length))
                                            .toDouble()),
                                  if (media.length == 3)
                                    Image.file(
                                      media[2],
                                      // height: 105,
                                      width: 105,
                                    ),
                                  if (media.length >= 4)
                                    Stack(
                                        alignment: Alignment.center,
                                        children: <Widget>[
                                          ColorFiltered(
                                            colorFilter: ColorFilter.mode(
                                              Colors.grey.withOpacity(0.85),
                                              BlendMode.darken,
                                            ),
                                            child: Image.file(
                                              media[2],
                                              height: 105,
                                              width: 105,
                                            ),
                                          ),
                                          Text(
                                            (media.length - 3).toString() + "+",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ]),
                                ],
                              )),
                  ]),
                ),
              )),
        ),
      ),
    );
  }
}
