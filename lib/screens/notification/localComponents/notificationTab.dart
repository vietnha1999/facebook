import 'package:facebook/api/notification/notificationApi.dart';
import 'package:facebook/middlewares/friendsMiddleware.dart';
import 'package:facebook/middlewares/notificationMiddleware.dart';
import 'package:facebook/middlewares/userInfoMiddleware.dart';
import 'package:facebook/screens/notification/localComponents/notificationList.dart';
import 'package:facebook/screens/notification/localComponents/notificationTopBar.dart';
import 'package:facebook/screens/notification/localComponents/suggestMakeFriendBottomBar.dart';
import 'package:facebook/screens/notification/localComponents/suggestMakeFriendList.dart';
import 'package:facebook/screens/notification/localComponents/suggestMakeFriendTopBar.dart';
import 'package:facebook/screens/notification/notificationConst.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/notificationState.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class NotificationTab extends StatelessWidget {
  final List<Map<String, String>> dataNotify;
  final List<Map<String, String>> dataSuggest;
  NotificationTab({@required this.dataNotify, @required this.dataSuggest});
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        NotificationTopBar(),
        SliverList(
          delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
            return Container(
              alignment: Alignment.centerLeft,
              height: 30,
              margin: EdgeInsets.fromLTRB(16, 0, 0, 0),
              child: Text("Mới", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: NOTIFICATION_FONT_SIZE_CONTENT))
            );
          },
          childCount: 1,
        )),
        StoreConnector<AppState, List<NotificationSlotState>>(
          distinct: true,
          converter: (store) => store.state.listNotifications,
          onInit: (store) async {
            await StoreProvider.of<AppState>(context).dispatch(getNotificationsThunk());
            // await StoreProvider.of<AppState>(context).dispatch(getUserInfoThunk(USER_ID));
            // await StoreProvider.of<AppState>(context).dispatch(getFriendsThunk(USER_ID));
          },
          builder: (context, viewModel) {
            return NotificationList(data: dataNotify.sublist(6, 10), dataNotify: viewModel);
          }
        )
        // SuggestMakeFriendTopBar(),
        // SuggestMakeFriendList(data: dataSuggest),
        // SuggestMakeFriendBottomBar(),
        // SliverList(
        //   delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        //     return Container(
        //       height: 20,
        //       margin: EdgeInsets.fromLTRB(16, 13, 0, 0),
        //       child: Text("Trước đó", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: NOTIFICATION_FONT_SIZE_CONTENT)),
        //     );
        //   },
        //   childCount: 1,
        // )),
        // NotificationList(data: dataNotify)
      ]
    );
  }
}