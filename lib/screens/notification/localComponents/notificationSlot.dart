import 'dart:convert';
import 'dart:developer';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:facebook/common/constants.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/notification/notificationConst.dart';
import 'package:facebook/stores/notificationState.dart';
import 'package:flutter/material.dart';

class NotificationSlot extends StatelessWidget {
  final NotificationSlotState data;
  NotificationSlot({@required this.data});
  String _getTextAction() {
    switch(data.type) {
      case "request":
        return " đã gửi lời mời kết bạn";
      case "accept":
        return " đã chấp nhận lời mời kết bạn";
      case "comment":
        return " đã bình luận về bài viết có mặt bạn";
      case "like":
        return " đã thích bài viết của bạn";
      default:
        return " UN_SUPPORTED ";
    }
  }
  void _onTap(BuildContext context) {
    log("Tap notification slot");
    switch(data.type) {
      case "request":
      case "accept":
        Navigator.pushNamed(context, PERSONAL_PAGE, arguments: {"id": int.parse(data.entityId)});
        log(jsonEncode({"id": int.parse(data.entityId)}));
        // NavService.navigateTo(MESSENGER_MAIN);
        break;
      case "comment":
        break;
      case "like":
        break;
    }
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      customBorder: ContinuousRectangleBorder(),
      onTap: () {
        _onTap(context);
      },
      child: Container(
        color: ("1" != "1")? Colors.transparent : const Color.fromRGBO(142, 190, 240, 0.25),
        height: 60,
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(8, 4, 5, 12),
              width: 40,
              child: Stack(
                children: [
                  ClipOval(
                    child: CachedNetworkImage(
                      imageUrl: FileURI+data.avatarUrl,
                      errorWidget: (context, url, error) => CircularProgressIndicator(),
                      fit: BoxFit.cover,
                      width: 40,
                      height: 40,
                    )
                  ),
                  // Positioned(
                  //   bottom: -2,
                  //   right: -2,
                  //   child: ClipOval(
                  //     child: CachedNetworkImage(
                  //       imageUrl: "https://facebook.com/rsrc.php/v3/yh/r/2VIAxxDDgUf.png",
                  //       fit: BoxFit.fitWidth,
                  //       width: 10,
                  //       height: 10,
                  //     )
                  //   ),
                  // )
                ],
                overflow: Overflow.visible,
              )
            ),
            Expanded(
              child:
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        style: TextStyle(color: Colors.black),
                        children: [
                          TextSpan(text: data.name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: NOTIFICATION_FONT_SIZE_CONTENT)),
                          TextSpan(text: _getTextAction(), style: TextStyle(fontSize: NOTIFICATION_FONT_SIZE_CONTENT)),
                          TextSpan(text: "", style: TextStyle(fontWeight: FontWeight.bold, fontSize: NOTIFICATION_FONT_SIZE_CONTENT))
                        ]
                      )
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text('24 tháng 9 lúc 20:30', style: TextStyle(fontWeight: FontWeight.w300, fontSize: NOTIFICATION_FONT_SIZE_TIME))
                    )
                  ]
                )
            ),
            Container(
              margin: EdgeInsets.fromLTRB(3, 3, 0, 35),
              child: InkWell(
                customBorder: CircleBorder(),
                child: Container(
                  width: 40,
                  height: 40,
                  padding: EdgeInsets.only(bottom: 4),
                  child: Center(child: Text("...", style: TextStyle(fontWeight: FontWeight.bold, fontSize: NOTIFICATION_FONT_SIZE_CONTENT))),
                ),
                onTap: () => log("Tap options")
              )
            )
          ],
        ),
      )
    );
  }
}