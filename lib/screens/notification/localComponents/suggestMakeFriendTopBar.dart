import 'dart:developer';
import 'package:facebook/screens/notification/notificationConst.dart';
import 'package:flutter/material.dart';

class SuggestMakeFriendTopBar extends StatelessWidget {
  SuggestMakeFriendTopBar();
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        return Container(
          height: 40,
          margin: EdgeInsets.fromLTRB(16, 0, 0, 0),
          child:
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Divider(endIndent: 16, height: 1, thickness: 1),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text("Những người bạn có thế biết", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: NOTIFICATION_FONT_SIZE_CONTENT)),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      child: InkWell(
                        customBorder: CircleBorder(),
                        child: Container(
                          alignment: Alignment.center,
                          width: 20,
                          height: 20,
                          child: Center(child: Text("Ẩn", style: TextStyle(color: Colors.blue, fontSize: NOTIFICATION_FONT_SIZE_CONTENT))),
                        ),
                        onTap: () => log("Tap hide")
                      )
                    )
                  ]
                )
              ]
          )
        );
      },
      childCount: 1,
    ));
  }
}