import 'package:facebook/actions/exampleActions.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/exampleState.dart';
import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';

class SetDarkModeViewModel {
  final bool darkMode;
  final Function(bool darkMode) dispatch;

  SetDarkModeViewModel({@required this.darkMode, @required this.dispatch});

  static SetDarkModeViewModel fromStore(Store<AppState> store) {
    return SetDarkModeViewModel(
      darkMode: store.state.userUIState.darkMode,
      dispatch: (bool darkMode) {
        store.dispatch(SetDarkModeAction(darkMode));
      },
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SetDarkModeViewModel && darkMode == other.darkMode;

  @override
  int get hashCode => darkMode.hashCode;
}

class SetExampleProductViewModel {
  final Function(String name) dispatch;

  SetExampleProductViewModel({@required this.dispatch});

  static SetExampleProductViewModel fromStore(Store<AppState> store) {
    return SetExampleProductViewModel(dispatch: (String name) {
      store.dispatch(SetExampleProductAction(ExampleProduct(name: name)));
    });
  }
}