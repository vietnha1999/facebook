import 'dart:developer';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/example/exampleViewModel.dart';
import 'package:facebook/stores/appState.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ExampleScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(StoreProvider.of<AppState>(context).state.deviceInfo.uuid),
        ),
        body: Center(
            child: Column(
          children: <Widget>[
            StoreConnector<AppState, SetDarkModeViewModel>(
                distinct: true,
                converter: (store) {
                  return SetDarkModeViewModel.fromStore(store);
                },
                builder: (context, viewModel) {
                  return Checkbox(
                    value: viewModel.darkMode,
                    onChanged: (bool value) {
                      log("value changed" + value.toString());
                      viewModel.dispatch(value);
                    },
                  );
                }),
            StoreConnector<AppState, SetExampleProductViewModel>(
                distinct: true,
                converter: (store) {
                  return SetExampleProductViewModel.fromStore(store);
                },
                builder: (context, viewModel) {
                  return TextField(
                    maxLength: 10,
                    onSubmitted: (String value) {
                      log("value input" + value);
                      viewModel.dispatch(value);
                    },
                  );
                })
          ],
        )),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.pushNamed(context, EXAMPLE_RESULT);
          },
        ));
  }
}

class ExampleResult extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Resultxx'),
          automaticallyImplyLeading: false,
        ),
        body: Center(
            child: ListView.builder(
                physics: AlwaysScrollableScrollPhysics(),
                padding: const EdgeInsets.all(10),
                addAutomaticKeepAlives: true,
                itemCount: 1,
                itemBuilder: (BuildContext context, int index) {
                  return StoreConnector<AppState, String>(
                    onDispose: (store) => log("onDispose" + store.toString()),
                    onDidChange: (store) =>
                        log("onDidChange" + store.toString()),
                    onInit: (store) => log("onInit" + (store.toString())),
                    onInitialBuild: (store) =>
                        log("onInitialBuild" + (store.toString())),
                    onWillChange: (store, store1) => log("onWillChange" +
                        (store.toString()) +
                        "__" +
                        (store1).toString()),
                    distinct: true,
                    converter: (store) {
                      return store.state.exampleProduct.name;
                    },
                    builder: (context, viewModel) {
                      return Container(
                        height: 50,
                        color: Colors.amber[0],
                        child: Center(
                            child: Text(
                          viewModel,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                      );
                    },
                  );
                })));
  }
}
