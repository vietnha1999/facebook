import 'package:facebook/middlewares/toDoMiddleware.dart';
import 'package:facebook/middlewares/toDoMiddleware.dart';
import 'package:facebook/stores/appState.dart';
import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';

class AlbumByUserIdViewModel {
  final Function(String id) dispatch;

  AlbumByUserIdViewModel({@required this.dispatch});

  static AlbumByUserIdViewModel fromStore(Store<AppState> store) {
    return AlbumByUserIdViewModel(dispatch: (String id) {
      store.dispatch(getAlbumByUserIdThunk(id));
    });
  }
}
