import 'package:flutter/material.dart';
import 'package:facebook/router/Router.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Home"),
        ),
        body: Center(
            child: Column(
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, TO_DO_INPUT);
              },
              child: Text("To Do Input")
            ),
            RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, EXAMPLE_SCREEN);
              },
              child: Text("Example Screen")
            ),
            RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, CHAT_SCREEN);
              },
              child: Text("Chat")
            )
          ],
        )));
  }
}
